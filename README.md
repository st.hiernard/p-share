# P-Share

Application web de partage d'idées de projet fait avec React.js.

Contribution personnelle : Front-end

## Installation

### Dépendances

Installer `nodejs` et `npm` :
voir la page d'installation de [Node.js](https://nodejs.org/fr/download/).


Installer les dépendances du projet :

```
npm install [--production]
```

### Configuration - Variables d'environnement

|    Variable    |       Description       |
|:-------------- |:----------------------- |
| `backend_port` | Port du serveur Express |

### Configuration - Identifiants de base de données

Les identifiants d'accès à la base de données doivent être renseignés dans le
fichier `config.js`.

Un exemple est donné dans le fichier `example-config.js`.

### Run!

```sh
# En mode de production
npm start
# En mode de développement
npm run dev
```
