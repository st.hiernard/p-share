const initialState = {
  key: 'vote',
  order: 'descending'
};

const reducer = (state = initialState, action) => {
  const newState = {...state};

  if (action.type === 'SET_SORTING') {
    newState.key = action.value.key;
    newState.order = action.value.order;
  }

  return newState;
};

export default reducer;
