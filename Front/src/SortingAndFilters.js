import React, { Component, useState, useEffect } from 'react';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Box from '@material-ui/core/Box';
import Slider from '@material-ui/core/Slider';
import { useDispatch } from 'react-redux';

const SortingAndFiltersCSS = {
	display: 'flex',
	flexDirection: 'row'
}

const SortingSectionCSS = {
	
}

const FilterSectionCSS = {
	display: 'flex',
	flexDirection: 'row-reverse',
	flexGrow: 1,
}

const FilterSectionDivCSS = {
	display: 'flex',
	flexDirection: 'row-reverse',
	alignItems: 'center'

}

const SliderCSS = {
	width: '75%'
}

function SortingAndFilters(){
	return (
		<Box p={1} style={SortingAndFiltersCSS} className="sort-filter">
			<Box p={1} order={1} style={SortingSectionCSS} className="sorting">
				<ProjectSorting/>
			</Box>
			<Box p={1} order={2} style={FilterSectionCSS} className="filters">
				<ProjectFilters/>
			</Box>
		</Box>
	);
}

function ProjectSorting(){
	const [sortBy, setSortBy] = React.useState(1);
	const dispatch = useDispatch();

	const handleChange = (event) => {
		setSortBy(event.target.value);
	};

  useEffect(() => {
    let value = {
      key: undefined,
      order: undefined
    };

    switch (sortBy) {
      case 1:
        value.key = 'vote';
        value.order = 'descending';
        break;
      case 2:
        value.key = 'vote';
        value.order = 'ascending';
        break;
      case 3:
        value.key = 'publication';
        value.order = 'descending';
        break;
      case 4:
        value.key = 'publication';
        value.order = 'ascending';
        break;
    }

    dispatch({
      type: 'SET_SORTING',
      value: value
    });

    dispatch({ type: 'UPDATER_READY', value: 'sort' });
  }, [sortBy]);

	return(
		<div>
			<InputLabel>Trier par :</InputLabel>
			<Select
			  value={sortBy}
			  onChange={handleChange}
			>
			  <MenuItem value={1}>Popularité décroissante</MenuItem>
			  <MenuItem value={2}>Popularité croissante</MenuItem>
			  <MenuItem value={3}>Date de publication décroissante</MenuItem>
			  <MenuItem value={4}>Date de publication croissante</MenuItem>
			</Select>
		</div>
	);
}

function ProjectFilters(){
	return(
		<Box style={FilterSectionDivCSS}>
			<Box m={1}>
				<FiliereFilter/>
			</Box>
			<Box m={1}>
				<StatusFilter/>
			</Box>
			<Box m={1}>
				<TypeFilter/>
			</Box>
			<Box m={1}>
				<ParticipantsFilter/>
			</Box>
		</Box>
	);
}

function FiliereFilter() {
	const [filterBy, setFilterBy] = React.useState('*');
	const dispatch = useDispatch();
	
	const handleChange = (event) => {
		setFilterBy(event.target.value);
	};
	
	useEffect(() => {
    let value = {
      key: undefined
    };
	
	if(filterBy == "*"){
		value.key = undefined;
	}else{
		value.key = filterBy;
	}
	
	dispatch({
      type: 'SET_FILIERE_FILTERING',
      value: value
    });

    dispatch({ type: 'UPDATER_READY', value: 'filiere' });
  }, [filterBy]);
	
	return(
		<div>
			<InputLabel>Filière :</InputLabel>
			<Select
			  value={filterBy}
			  onChange={handleChange}
			>
			  <MenuItem value={'*'}>Tous</MenuItem>
			  <MenuItem value={'GB'}>GB</MenuItem>
			  <MenuItem value={'GI'}>GI</MenuItem>
			  <MenuItem value={'GP'}>GP</MenuItem>
			  <MenuItem value={'GU'}>GU</MenuItem>
			  <MenuItem value={'HuTech'}>HuTech</MenuItem>
			  <MenuItem value={'IM'}>IM</MenuItem>
			  <MenuItem value={'TC'}>TC</MenuItem>
			  
			</Select>
		</div>
	);
}

function StatusFilter() {
	const [filterBy, setFilterBy] = React.useState('*');
	const dispatch = useDispatch();
	
	const handleChange = (event) => {
		setFilterBy(event.target.value);
	};
	
	useEffect(() => {
    let value = {
      key: []
    };
	
	if(filterBy == "*"){
		value.key = [];
	}else{
		value.key = [filterBy];
	}
	
	dispatch({
      type: 'SET_STATUS_FILTERING',
      value: value
    });

    dispatch({ type: 'UPDATER_READY', value: 'status' });
  }, [filterBy]);
	
	return(
		<div>
			<InputLabel>Phase du projet :</InputLabel>
			<Select
			  value={filterBy}
			  onChange={handleChange}
			>
			  <MenuItem value={'*'}>Tous</MenuItem>
			  <MenuItem value={'idea'}>Idée</MenuItem>
			  <MenuItem value={'definition'}>Définition</MenuItem>
			  <MenuItem value={'specification'}>Spécification</MenuItem>
			  <MenuItem value={'conception'}>Conception</MenuItem>
			  <MenuItem value={'development'}>Développement</MenuItem>
			  <MenuItem value={'test'}>Test</MenuItem>
			  <MenuItem value={'production'}>Production</MenuItem>
			</Select>
		</div>
	);
}

function TypeFilter() {
	const [filterBy, setFilterBy] = React.useState('*');
	const dispatch = useDispatch();
	
	const handleChange = (event) => {
		setFilterBy(event.target.value);
	};
	
	useEffect(() => {
    let value = {
      key: []
    };
	
	if(filterBy == "*"){
		value.key = [];
	}else{
		value.key = [filterBy];
	}
	
	dispatch({
      type: 'SET_TYPE_FILTERING',
      value: value
    });

    dispatch({ type: 'UPDATER_READY', value: 'type' });
  }, [filterBy]);
	
	return(
		<div>
			<InputLabel>Type du projet :</InputLabel>
			<Select
			  value={filterBy}
			  onChange={handleChange}
			>
			  <MenuItem value={'*'}>Tous</MenuItem>
			  <MenuItem value={'PA'}>Projet Associatif</MenuItem>
			  <MenuItem value={'PP'}>Projet Personnel</MenuItem>
			  <MenuItem value={'UV'}>UV</MenuItem>
			  <MenuItem value={'TX'}>TX</MenuItem>
			  <MenuItem value={'PR'}>PR</MenuItem>
			</Select>
		</div>
	);
}

function ParticipantsFilter() {
	const MIN = 0;
	const MAX = 20;
	
	const dispatch = useDispatch();
	const [members, setMembers] = React.useState([MIN, MAX]);

	const handleChange = (event, newValue) => {
		  setMembers(newValue);  
	};
	
	useEffect(() => {
		dispatch({ type: 'UPDATER_READY', value: 'participants' });
	}, []);

	const dispatchFilter = (_, v) => {
		let value = { min: v[0], max: v[1] };

		dispatch({
			type: 'SET_PARTICIPANTS_FILTERING',
			value: value
		});
	}

	return (
	<div>
	  <InputLabel>Nombre de participants :</InputLabel>
	  <Slider
		style={SliderCSS}
		value={members}
		onChange={handleChange}
		valueLabelDisplay="auto"
		aria-labelledby="range-slider"
		min={MIN}
		max={MAX}
		onChangeCommitted={dispatchFilter}
	  />
	</div>
	);
}


export default SortingAndFilters;