import { createStore, combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';

import authReducer from './authReducer';
import listSortingReducer from './listSortingReducer';
import { filiereReducer, statusReducer, typeReducer, participantsReducer, projectListUpdaterReducer } from './filterReducer';

const reducer = combineReducers({
  form: reduxFormReducer, // mounted under "form"
  auth: authReducer,
  sort: listSortingReducer,
  filiere: filiereReducer,
  status: statusReducer,
  participants: participantsReducer,
  type: typeReducer,
  updater: projectListUpdaterReducer
});
const store = (window.devToolsExtension
  ? window.devToolsExtension()(createStore)
  : createStore)(reducer);

export default store;
