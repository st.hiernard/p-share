import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import Grid from '@material-ui/core/Grid';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import Box from '@material-ui/core/Box';
import { flexbox } from '@material-ui/system';
import GroupIcon from '@material-ui/icons/Group';
import TimerIcon from '@material-ui/icons/Timer';
import Button from '@material-ui/core/Button';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton';
import axios from "axios";
import toastr from "toastr";

import {
	BrowserRouter as Router,
	Link
} from "react-router-dom";

const ProjectCardCSS = {
	marginBottom: 10
}

const ContentTileCSS = {
	display: 'flex',
	flexDirection : 'row'
}

const VoteSystemCSS = {
	marginRight: 10,
	display: 'flex',
	flexDirection : 'column',
	alignItems: 'center',
	justifyContent: 'center',
	alignContent: 'center'
}

const ProjetCardContentCSS = {
	marginTop: 10,
	width: '100%'
}

const ProjectInfosCSS = {
	display: 'flex',
	flexDirection : 'row',
	alignItems: 'center',
	marginTop: 5,
}

const ProjectDescriptionCSS = {
	textAlign: 'justify'
}

const InfoIconCSS = {
	marginRight: 5,
	marginLeft: 10
}

const SubscribeButtonCSS = {
	display: 'flex',
	justifyContent: 'flex-end'
}

const ProjectTagsCSS = {
	display: 'flex',
	flexDirection : 'row',
	justifyContent: 'flex-end',
	height: 30
}

const ProjectHeadCSS = {
	display: 'flex',
	flexDirection : 'row',
	alignItems: 'center',
	marginBottom: 10,
	height: 20
}

const ProjectTagDefaultCSS = {
	marginLeft: 5,
	backgroundColor: '#00bcd4',
	color: 'white'
}
const ProjectTagThemeCSS = {
	marginLeft: 5,
	backgroundColor: '#8bc34a',
	color: 'white'
}
const ProjectTagTechnosCSS = {
	marginLeft: 5,
	backgroundColor: 'teal',
	color: 'white'
}

const TextLinkCSS = {
	textDecoration: "inherit",
	color: 'inherit'
}

class ProjectTile extends React.Component {
	
	/*Props:
		projectID, //Non affiché
		date,
		vote,
		nbParticipants,
		delai,
		projectTitle,
		projectDescription,
		tags //liste
	*/

	constructor(props) {
		super(props);
		this.state = {
			vote : this.props.vote,
			cssSubscribe: this.props.isSubscribed === 1 ? { backgroundColor: '#7f7f7f' } : {},
			cssUpvote: this.props.currentVote === 1 ? { color: '#00df00' } : {},
			cssDownvote: this.props.currentVote === -1 ? { color: '#df0000' } : {},
			subscribeText: this.props.isSubscribed === 1 ? 'Se désabonner' : 'S\'abonner',
			isSubscribed: this.props.isSubscribed,
			currentVote: this.props.currentVote
		}
	}

	getDateInFormat() {
		return new Intl.DateTimeFormat().format(new Date(this.props.date));
	}
	
	delayToStr(seconds){
		if(seconds === 0 || !seconds){
			return "Non défini"
		}
		seconds = Number(seconds);
		var y = Math.floor(seconds / (3600*24*365));
		var m = Math.floor((seconds % (3600*24*365)) / (30*24*3600));
		var j = Math.floor((seconds % (30*24*3600)) / (3600*24));

		var yDisplay = y > 0 ? y + (y == 1 ? " année, " : " années, ") : "";
		var mDisplay = m > 0 ? m + " mois, " : "";
		var jDisplay = j > 0 ? j + (j == 1 ? " jour" : " jours") : "";

		return yDisplay + mDisplay + jDisplay;
	}
	
	setCSSUpvote = () => {
		/*axios.post('api/project/' + this.props.projectID + '/upvote').then(res => {
			if(res.alreadyvoted){
				this.setState({
					cssUpvote: {color: 'primary'}
				});
			}
		});
		*/
	};
	
	setCSSDownvote = () => {
		/*axios.post('api/project/' + this.props.projectID + '/downvote').then(res => {
			if(res.alreadyvoted){
				this.setState({
					cssUpvote: {color: 'red'}
				});
			}
		});
		*/
	};

	upvote = () => {
		if (this.state.currentVote !== 0) {
			// Reset vote
			axios.post('api/project/' + this.props.projectID + '/resetvote').then(res => {
				toastr.success('Vote annulé');
				this.setState({
					vote: res.data.vote,
					cssUpvote: {},
					cssDownvote: {},
					currentVote: 0
				});
			});
		} else {
			// Send upvote
			axios.post('api/project/' + this.props.projectID + '/upvote').then(res => {
				toastr.success('Vote enregistré');
				this.setState({
					vote: res.data.vote,
					cssUpvote: { color: '#00df00' },
					cssDownvote: {},
					currentVote: 1
				});
			});
		}
	};

	downvote = () => {
		if (this.state.currentVote !== 0) {
			// Reset vote
			axios.post('api/project/' + this.props.projectID + '/resetvote').then(res => {
				toastr.success('Vote annulé');
				this.setState({
					vote: res.data.vote,
					cssUpvote: {},
					cssDownvote: {},
					currentVote: 0
				});
			});
		} else {
			// Send downvote
			axios.post('api/project/' + this.props.projectID + '/downvote').then(res => {
				toastr.success('Vote enregistré');
				this.setState({
					vote: res.data.vote,
					cssUpvote: {},
					cssDownvote: { color: '#df0000' },
					currentVote: -1
				});
			});
		}
	};

	subscribe = () => {
		if (this.state.isSubscribed === 1) {
			axios.post('/api/project/' + this.props.projectID + '/unsubscribe').then(res => {
				toastr.success('Vous n\'êtes plus abonné !');
				this.setState({
					cssSubscribe: {},
					subscribeText: 'S\'abonner',
					isSubscribed: 0
				});
			});
		} else {
			axios.post('api/project/' + this.props.projectID + '/subscribe').then(res => {
				toastr.success('Vous êtes abonné !');
				this.setState({
					cssSubscribe: { backgroundColor: '#7f7f7f' },
					subscribeText: 'Se désabonner',
					isSubscribed: 1
				});
			});
		}
	};
	
	render(){
		return (
		  <Card className={"ProjectCard"} style={ProjectCardCSS}>

			<CardContent>
				<div className={"ProjectCardHead"}>	
					<Box style={ProjectHeadCSS}>
						<Box order={1}>
							<Typography variant={"overline"} gutterBottom>
								Publié le {this.getDateInFormat()}
							</Typography>
						</Box>
						<Box order={2} flexGrow={1} style={ProjectTagsCSS} className="tags-project">
							{this.props.tags == null? null:
								this.props.tags.map((value, index) => {
									try{
										if(value.value == null) return;
										switch(value.category){
										case "theme":
											return <Button key={value.value} style={ProjectTagThemeCSS} variant="contained" color="secondary" size="small" disabled>{value.value}</Button>
											break;
										case "technos":
											return <Button key={value.value} style={ProjectTagTechnosCSS} variant="contained" size="small" disabled>{value.value}</Button>
											break;
										default:
											return <Button key={value.value} style={ProjectTagDefaultCSS} variant="contained" color="secondary" size="small" disabled>{value.value}</Button>
									}
									}catch(error){
										return null;
									}
							})}
							<IconButton aria-label="Options" size="small">
								<MoreVertIcon/>
							</IconButton>
						</Box>
					</Box>
				</div>
				
				<Divider light />
				
				<div style= {ContentTileCSS} className="project-content">
				
					<Box p={0} style={VoteSystemCSS}>
						<Box p={0} order={1}>
							<IconButton aria-label="Upvote" size="small" onClick={this.upvote} style={this.state.cssUpvote}>
								<ArrowUpwardIcon/>
							</IconButton>
						</Box>
						<Box p={0} order={2}>
							<Typography variant={"subtitle2"} >
								{this.state.vote}
							</Typography>
						</Box>
						<Box p={0} order={3}>
							<IconButton aria-label="Downvote" size="small" onClick={this.downvote} style={this.state.cssDownvote}>
								<ArrowDownwardIcon/>
							</IconButton>
						</Box>
					</Box>
					
					<div style={ProjetCardContentCSS}>
						<Typography variant={"h5"} gutterBottom>
							<Link to={{pathname:`/ProjectPage/${this.props.projectID}`, aboutProps: this.props.projectID}} style={TextLinkCSS}>{this.props.projectTitle}</Link>
						</Typography>
						<Typography style={ProjectDescriptionCSS}>
							{this.props.projectDescription}{this.props.projectDescription.length>=254?'...':''}
						</Typography>
						<Box style={ProjectInfosCSS} class="bottom-project">
							<Box order={1} style={ProjectInfosCSS}>
								<GroupIcon style={InfoIconCSS}/>
								<Typography variant={"overline"}>
									{this.props.nbParticipants} Participants
								</Typography>
							</Box>
							<Box order={2} style={ProjectInfosCSS}>
								<TimerIcon style={InfoIconCSS}/>
								<Typography variant={"overline"}>
									Délai estimé : {this.delayToStr(this.props.delai)}
								</Typography>
							</Box>
							<Box order={3} flexGrow={1} style={SubscribeButtonCSS}>
								<Button variant="contained" color="primary" onClick={this.subscribe} style={this.state.cssSubscribe}>
									{this.state.subscribeText}
								</Button>
							</Box>
						</Box>
					</div>
				</div>
			</CardContent>
		  </Card>
	  )
	}
}

export const ProjectTileExample = () => (
  <Card className={"ProjectCard"} style={ProjectCardCSS}>

    <CardContent>
	    <div className={"ProjectCardHead"}>	
			<Box style={ProjectHeadCSS}>
				<Box order={1}>
					<Typography variant={"overline"} gutterBottom>
						Publié le 10/07/2020
					</Typography>
				</Box>
				<Box order={2} flexGrow={1} style={ProjectTagsCSS}>
					<Button style={ProjectTagDefaultCSS} variant="contained" color="secondary" size="small">
						GI
					</Button>
					<Button style={ProjectTagThemeCSS} variant="contained" color="secondary" size="small">
						Dev. WEB
					</Button>
					<IconButton aria-label="Options" size="small">
						<MoreVertIcon/>
					</IconButton>
				</Box>
			</Box>
		</div>
		
		<Divider light />
		
		<div style= {ContentTileCSS}>
		
			<Box style={VoteSystemCSS}>
				<Box order={1}>
					<IconButton aria-label="Upvote" size="small">
						<ArrowUpwardIcon/>
					</IconButton>
				</Box>
				<Box order={2}>
					<Typography variant={"subtitle2"} >
						12
					</Typography>
				</Box>
				<Box order={3}>
					<IconButton aria-label="Downvote" size="small">
						<ArrowDownwardIcon/>
					</IconButton>
				</Box>
			</Box>
			
			<div style={ProjetCardContentCSS}>
				<Typography variant={"h5"} gutterBottom>
				Project P-Share
				</Typography>
				<Typography style={ProjectDescriptionCSS}>
					Cet outil permettra de partager des idées de projets (pas qu’en informatique) entre les étudiants de l’UTC, pour ainsi faciliter la création ou le développement de projets. L’idée est de relier ceux qui ont des idées de projets avec Cet outil permettra de partager des idées de projets (pas qu’en informatique) entre les étudiants de l’UTC, pour ainsi...
				</Typography>
				<Box style={ProjectInfosCSS}>
					<Box order={1} style={ProjectInfosCSS}>
						<GroupIcon style={InfoIconCSS}/>
						<Typography variant={"overline"}>
							4 Participants
						</Typography>
					</Box>
					<Box order={2} style={ProjectInfosCSS}>
						<TimerIcon style={InfoIconCSS}/>
						<Typography variant={"overline"}>
							Délai estimé : 3 mois
						</Typography>
					</Box>
					<Box order={3} flexGrow={1} style={SubscribeButtonCSS}>
						<Button variant="contained" color="primary">
							S'abonner
						</Button>
					</Box>
				</Box>
			</div>
		</div>
    </CardContent>
  </Card>
);

export default ProjectTile;
