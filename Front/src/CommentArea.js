import React from "react";
import Comment from './Comment';
import { makeStyles } from "@material-ui/styles";


const useStyles = makeStyles((theme) => ({
    comments: {
        paddingBottom: 10,
        paddingTop: 10,
        marginBottom: 20,
		marginTop: 10,
        borderRadius: 5,
        backgroundColor: '#87CEFA'
    }
}));

function CommentArea ({projectID}) {
    const classes = useStyles();
    return (       
        <div className={classes.comments}>
            <Comment projectID={projectID}/>
        </div>
    );
} 


export default CommentArea;