import React, { Component } from 'react';
import MaterialUiForm from './MaterialUiForm';
import Container from '@material-ui/core/Container';

const FormPageCSS = {
	display: 'flex',
	justifyContent: 'center'
}

function FormPage() {
	return (
		<Container style={FormPageCSS}>
			<MaterialUiForm />
		</Container>
	);
}

export default FormPage;