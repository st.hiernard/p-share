import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { useSelector } from 'react-redux';


function TopBar() {
  return (
		<div>
			<ButtonAppBar />
		</div>
  );
}

function Logo() {
  return (
	  <div>
		<Link to="/" >
		  <img style={{height: '90px'}} src={process.env.PUBLIC_URL + '/img/P-Share_Logo_v3.png'} alt="P-Share Logo" />
		</Link>
	  </div>
  );
}

function UserMenu() {
  const displayName = useSelector(state => state.auth.displayName);
  return (
    <div>
      <Button color="inherit">{displayName}</Button>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
  },
}));


function ButtonAppBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" color="primary">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
		  <Logo/>
          </Typography>
          <UserMenu />
        </Toolbar>
      </AppBar>
    </div>
  );
}

const mapStateToProps = (state) => {
  const newState = {
    displayName: state.auth.displayName
  }

  return newState;
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default TopBar;
