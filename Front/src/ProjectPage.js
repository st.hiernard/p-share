import React, { useState, useEffect } from 'react';
import Divider from "@material-ui/core/Divider";
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import Participant from './Participant';
import PresentationProject from './PresentationProject';
import { makeStyles } from "@material-ui/styles";
import DetailledProject from "./DetailledProject";
import CommentArea from "./CommentArea";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: 20,
		display: 'flex',
		flexDirection: 'column'
    },
    main: {
        display: 'flex',
		flexDirection: 'row'
    },
	detailedSection: {
		flexGrow: 1
	},
	presentationProject:{
		height: '100%'
	},
	participants: {
		margin: '100px'
	},
    comment: {
		
    }
}));

function ProjectPage ({match}){
    const {projectID} = match.params;
    const classes = useStyles();
    const [project, setProject] = useState(null);

    useEffect(() => {
        axios('/api/project/' + projectID)
        .then((response) => {
            setProject(response.data);
        }).catch((error) => {
            console.log(error);
        });
    }, []);

    return (
        <FormControl className="full-project">
            
            <div>
            {project ?
                <Box className={classes.root}>
                
					<Box className={classes.main}>
						<PresentationProject className={classes.presentationProject} projectID={projectID} name={project.name} votes={project.votes} description={project.description} goal={project.goal} details={project.details} currentVote={project.current_vote}/>
						<Box className={classes.detailedSection}>
                            <DetailledProject projectID={projectID} tags={project.tags} publication={project.publication_date} duration={project.duration} status={project.status} isSubscribed={project.is_subscribed} authorEmail={project.author_email} authorName={project.author_display_name}/>
							<Divider light />
							<Participant projectID={projectID} nbParticipant={project.participants} authorName={project.author_display_name}/>
						</Box>
						
					</Box>
					
					<Divider light />
					
                    <div className={classes.comment}>
                    <CommentArea projectID={projectID} className="comments"/>
                    </div> 
				</Box>
            : ''}

            </div>



            
        </FormControl>
    );
} 


export default ProjectPage;