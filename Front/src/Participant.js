import React, { useState, useEffect } from 'react';
import axios from "axios";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import GroupIcon from '@material-ui/icons/Group';
import Button from '@material-ui/core/Button';
import MailIcon from '@material-ui/icons/Mail';
import { makeStyles } from "@material-ui/styles";
import TextField from '@material-ui/core/TextField';
import toastr from "toastr";
import { useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
    participants: {
        float: "right",
        width: '100%',
        borderRadius: 5,
        clear: "right",
        backgroundColor: "#ffffa7",
        paddingBottom: 10,
        marginTop: 10,
		marginBottom: 10,
    },
    participantTile: {
        marginLeft: 10,
        marginTop: 10,
        justifyContent: 'center',
        display: 'flex',
		
    },
    participantName: {
        textAlign: 'justify',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
    },
    infoIcon: {
        marginRight: 5,
        marginLeft: 5,
        marginTop: 3,
    },
    email: {
        marginLeft: 20,
        display: 'flex',
    },
	participation: {
		textAlign: 'center',
		margin: 1
    },
    formAddParticipant: {
        textAlign: "center",
    },
    formControl: {
        width: '75%',
    },
    button: {
        marginTop: 10,
        marginBottom: 10,
    },
    projectTag: {
        display: 'flex',
        margin: 5,
        justifyContent: 'center',
    }
}));

function Participant ({projectID, nbParticipant, authorName}){
        const classes = useStyles();
        const [children, setChildren] = useState([]);
        const [rendered, setRendered] = useState(false);
        const [fin, setFin] = useState(false);
    
        const [email, setEmail] = useState("");
        const [addParticipant, setAddParticipant] = useState(false);

        const displayName = useSelector(state => state.auth.displayName);
    
        const handleListParticipant = () => {
            if(!rendered && !fin){
                const fetchData = async () => {
                    const result = await axios(
                    '../api/project/'+projectID+'/participants'
                    );
                    var items = [];
                    result.data.forEach(element => {
                        items.push(element);
                    });
                    if(result.data.length === 0){
                        setFin(true);
                    }
                    var concatArray = children.concat(items);
                    setChildren(concatArray);
                }
                fetchData();
                setRendered(true);
            }
        };

        useEffect(() => {
            handleListParticipant()
        }, [rendered, fin]);

        const handleAddParticipant = (e) => {
            e.preventDefault();
            const email = e.target.elements.email.value.trim();
            if(email){
              axios.post('../api/project/' + projectID + '/add-participant/' + email)
              .then(res => {
                toastr.success('Participant ajouté');
                setEmail(res.data.email);
                setChildren([]);
                setRendered(false);
                setFin(false);
              });
              e.target.elements.email.value = "";
            }
            setAddParticipant(false);
          };
      
    
        return (
            <div className={classes.participants}>
                
                        <div className={classes.participantTile}>
                            <GroupIcon className={classes.infoIcon}/>
                            <Typography variant={"h5"} gutterBottom>
                                <b>Participants</b>
                            </Typography>
                        </div>
                        {children.length <= 0 ? <div className={classes.participantTile}><Divider/><Typography >Aucun participant</Typography></div> :
                        children.map((children) => {
                        return(<div> 
                        <div className={classes.participantName}>
                        <Divider/>
                        <Typography>
                            <b>{children.forename} {children.surname} ({children.role})</b>
                        </Typography>
                        </div>
                        <div className={classes.email}>
                            <MailIcon className={classes.infoIcon}/>
                            <Typography>
                            <em>{children.email}</em>
                            </Typography>
                        </div>
                        </div>
                        );
                        })}
                        {(displayName === authorName) &&
                            <div className={classes.formAddParticipant}>
                            <Button className={classes.button} variant="contained" color="default" onClick={() => setAddParticipant(true)}>
                                    Ajouter un participant
                            </Button>
                            {addParticipant && (
                                <form onSubmit={handleAddParticipant}>
                                <div>
                                    <FormControl className={classes.formControl}>
                                    <TextField
                                        name="email"
                                        label="Email"
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                    />
                                    </FormControl>
                                </div>
                                <div>
                                    <Button
                                    type="submit"
                                    variant="contained"
                                    color="default"
                                    className={classes.button}
                                    >
                                    Envoyer
                                    </Button>
                                </div>
                                </form>
                            )}
                        </div>
                        }
                        
						<Divider/>
						<Box className={classes.participation}>
							<Typography variant="caption">Veuillez contacter l'auteur du projet pour pouvoir y participer</Typography>
						</Box>
					</div>
        );
    }


export default Participant;