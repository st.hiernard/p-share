import React from 'react';
import TopBar from './TopBar';
import HomePage from './HomePage';
import FormPage from './FormPage';
import ProjectPage from './ProjectPage';
import LoggerPage from './LoggerPage';
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import './responsive.css';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { connect } from 'react-redux';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#6ec6ff',
      main: '#2196f3',
      dark: '#0069c0',
      contrastText: '#fff',
    },
    secondary: {
      light: '#c158dc',
      main: '#8e24aa',
      dark: '#5c007a',
      contrastText: '#fff',
    },
  },
});

const HomePageCSS = {
  backgroundColor: 'green'
}

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
          P-Share
      {' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

class App extends React.Component {
  render() {
    return <ThemeProvider theme={theme}>{this.props.content}</ThemeProvider>;
  }
}

const mapStateToProps = (state) => {
  const newState = {
    content: undefined
  }

  if (state.auth.loggedIn) {
    newState.content = (
      <Router>
        <TopBar />
        <Container>
          <Switch>
            <Route exact path="/" component={HomePage}/>
            <Route path="/FormPage" component={FormPage}/>
            <Route path="/ProjectPage/:projectID" component={ProjectPage}/>
          </Switch>
          <Copyright />
        </Container>
      </Router>
    );
  } else {
    newState.content = (
      <Router>
        <TopBar />
        <LoggerPage />
        <Copyright />
      </Router>
    );
  }

  return newState;
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

