import React from "react";
import Typography from "@material-ui/core/Typography";
import Box from '@material-ui/core/Box';
import TimerIcon from '@material-ui/icons/Timer';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/styles';
import axios from "axios";
import toastr from "toastr";
import { useSelector } from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


const classes = theme => ({
    details: {
        float: "right",
        borderRadius: 5,
        backgroundColor: "#ffffa7",
    },
    voteSystem: {
        marginRight: 10,
	    display: 'flex',
	    flexDirection : 'column',
	    alignItems: 'center',
	    justifyContent: 'center',
	    alignContent: 'center',
    },
    projectInfo: {
        display: 'flex',
	    flexDirection : 'row',
        alignItems: 'center',  
    },
    projectTag: {
        display: 'flex',
        flexDirection: 'row',
        margin: 5,
    },
	technologies:{
		display: 'flex', 
		flexDirection: 'column',
		alignItems: 'center',
    },
    email:{
        color: 'blue',
        textDecoration: 'underline',
    }
});

function DeleteProject ({projectID, authorName}){
    const displayName = useSelector(state => state.auth.displayName);
	
	const [open, setOpen] = React.useState(false);

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

    const handleDeleteProject = () => {
		setOpen(false);
        axios.post('/api/project/' + projectID + '/delete').then(res => {
            toastr.success('Projet supprimé');
            window.location.href = ('../');
        });
    };

    return(
        <>
        {(displayName === authorName) &&
            <Box p={1} order={10}>
            <Box order={1} className={classes.projectInfo}>
            <Button className={classes.projectTag} variant="contained" color="default" onClick={handleClickOpen}>
                Supprimer ce projet
            </Button>
			
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
			  >
				<DialogTitle id="alert-dialog-title">{"Supprimer ce projet?"}</DialogTitle>
				<DialogContent>
				  <DialogContentText id="alert-dialog-description">
					Etes-vous sûr de vouloir supprimer ce projet?
				  </DialogContentText>
				</DialogContent>
				<DialogActions>
				  <Button onClick={handleClose} color="primary">
					Non
				  </Button>
				  <Button onClick={handleDeleteProject} color="primary" autoFocus>
					Oui
				  </Button>
				</DialogActions>
			  </Dialog>
			
            </Box>
        </Box>
        }
        </>
    );
}


class DetailledProject extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            cssSubscribe: this.props.isSubscribed === 1 ? { backgroundColor: '#7f7f7f' } : {},
            subscribeText: this.props.isSubscribed === 1 ? 'Se\u00a0désabonner' : 'S\'abonner',
            isSubscribed: this.props.isSubscribed
        };
    }

    subscribe = () => {
        if (this.state.isSubscribed === 1) {
            axios.post('/api/project/' + this.props.projectID + '/unsubscribe').then(res => {
                toastr.success('Vous n\'êtes plus abonné !');
                this.setState({
                    cssSubscribe: {},
                    subscribeText: 'S\'abonner',
                    isSubscribed: 0
                });
            });
        } else {
            axios.post('/api/project/' + this.props.projectID + '/subscribe').then(res => {
                toastr.success('Vous êtes abonné !');
                this.setState({
                    cssSubscribe: { backgroundColor: '#7f7f7f' },
                    subscribeText: 'Se\u00a0désabonner',
                    isSubscribed: 1
                });
            });
        }
    };

    report = () => {
        axios.post('../api/project/' + this.props.projectID + '/report').then(res => {
            toastr.success('Projet signalé');
        });
    };

    getDateInFormat(){
        return new Intl.DateTimeFormat().format(new Date(this.props.publication));
    }
	
	delayToStr(seconds){
		if(seconds === 0 || !seconds){
			return "Non défini"
		}
		seconds = Number(seconds);
		var y = Math.floor(seconds / (3600*24*365));
		var m = Math.floor((seconds % (3600*24*365)) / (30*24*3600));
		var j = Math.floor((seconds % (30*24*3600)) / (3600*24));

		var yDisplay = y > 0 ? y + (y === 1 ? " année, " : " années, ") : "";
		var mDisplay = m > 0 ? m + " mois, " : "";
		var jDisplay = j > 0 ? j + (j === 1 ? " jour" : " jours") : "";

		return yDisplay + mDisplay + jDisplay;
	}


    render(){
        const {classes} = this.props;
        return (

            <div className={classes.details}>
                <Box p={1} className={classes.voteSystem}>
                    <Box p={1} order={1}>
                            <Box order={1} className={classes.projectInfo}>
                                <Typography>Créé par :&ensp;</Typography>
                                <Typography>{this.props.authorName}</Typography>
                            </Box>
                        </Box>
                        <Box p={1} order={2}>
                            <Box order={1} className={classes.projectInfo}>
                                <Typography className={classes.email}><a href={"mailto:"+ this.props.authorEmail}>{this.props.authorEmail}</a></Typography>
                            </Box>
                        </Box>
                    <Box p={1} order={3}><Box order={1} className={classes.projectInfo}>
                        <Typography>Filières&nbsp;:</Typography>
                        {this.props.tags.filter((tag) => tag.category === 'sector').length ? null : <Typography>aucune</Typography>}
                        {this.props.tags.filter((tag) => tag.category === 'sector').map((tag) => {
                            return (
                                <Button className={classes.projectTag} variant="contained" color="secondary" size="small">
                                    {tag.value}
                                </Button>
                            );
                        })}
                    </Box></Box>
                    <Box p={1} order={4}><Box order={1} className={classes.projectInfo}>
                        <Typography>Thèmes&nbsp;:</Typography>
                        {this.props.tags.filter((tag) => tag.category === 'theme').length ? null : <Typography>aucun</Typography>}
                        {this.props.tags.filter((tag) => tag.category === 'theme').map((tag) => {
                            return (
                                <Button className={classes.projectTag} variant="contained" color="secondary" size="small">
                                    {tag.value}
                                </Button>
                            );
                        })}
                    </Box></Box>
                    <Box p={1} order={5}><Box order={1} className={classes.projectInfo}>
                        <Typography>Technologies&nbsp;:</Typography>
                        {this.props.tags.filter((tag) => tag.category === 'technos').length ? null : <Typography>aucune</Typography>}
                        {this.props.tags.filter((tag) => tag.category === 'technos').map((tag) => {
                            return (
                                <Button className={classes.projectTag} variant="contained" color="secondary" size="small">
                                    {tag.value}
                                </Button>
                            );
                        })}
                    </Box></Box>
                        <Box p={1} order={6}>
                            <Box order={1} className={classes.projectInfo}>
                                <Typography>Publication :&ensp;</Typography>
                                <Typography>{this.getDateInFormat()}</Typography>
                            </Box>
                        </Box>

                        
                        
                        <Box p={1} order={7}>
                            <Box order={1} className={classes.projectInfo}>
                                <Typography>Délai estimé :&ensp;</Typography>
                                <TimerIcon className={classes.infoIcon}/>
                                <Typography>{this.delayToStr(this.props.duration)}</Typography>
                            </Box>
                        </Box>
                        <Box p={1} order={8}>
                            <Box order={1} className={classes.projectInfo}>
                                <Typography>Statut :&ensp;</Typography>
                                <Typography>{this.props.status}</Typography>
                            </Box>
                        </Box>
                        <Box p={1} order={9}>
                            <Box order={1} className={classes.projectInfo}>
                            <Button className={classes.projectTag} variant="contained" color="primary" onClick={this.subscribe} style={this.state.cssSubscribe}>
                                {this.state.subscribeText}
                            </Button>
                            <Button className={classes.projectTag} variant="contained" color="default" onClick={this.report}>
                                Signaler
                            </Button>
                            </Box>
                        </Box>

                        <DeleteProject projectID={this.props.projectID} authorName={this.props.authorName}/>
                        
                    </Box>
                    
                    </div>
        );
    }
}

export default withStyles(classes)(DetailledProject);