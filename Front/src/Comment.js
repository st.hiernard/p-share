import React, { useState, useEffect } from 'react';
import axios from "axios";
import toastr from "toastr";
import Button from '@material-ui/core/Button';
import ChatIcon from '@material-ui/icons/Chat';
import AddCommentIcon from '@material-ui/icons/AddComment';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
    formAddComment: {
        textAlign: "center",
      },
      commentTile: {
        marginLeft: 10,
        marginTop: 10,
        justifyContent: 'center',
        display: 'flex',
      },
      infoIcon: {
        marginRight: 5,
        marginLeft: 5,
        marginTop: 3,
      },
      formControl: {
        width: '75%',
      },
      button: {
        marginTop: 10,
        marginBottom: 10,
      },
}));

const CommentCardCSS = {
    marginTop: 10,
    width: '90%',
    marginLeft: '5%',
    marginRight: '5%',
}

const ProjetCardContentCSS = {
    marginTop: 10,
    width: '100%',
}

const CommentDateCSS = {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    height: 30
}

const CommentHeadCSS = {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
    height: 15
}

const CommentColorCSS = {
    backgroundColor: 'white',
    marginBottom: 10,
}

function GetDateInFormat(date){
return new Intl.DateTimeFormat().format(new Date(date));
}

function Comment ({projectID}) {
    const classes = useStyles();
    const [children, setChildren] = useState([]);
    const [rendered, setRendered] = useState(false);
    const [fin, setFin] = useState(false);

    const [comment, setComment] = useState("");

    const handleListComment = () => {

        if(!rendered && !fin){
            const fetchData = async () => {
                const result = await axios(
                    '../api/project/'+projectID+'/comments'
                );
                var items = [];
                result.data.forEach(element => {
                    items.push(element);
                });
                if(result.data.length === 0){
                    setFin(true);
                }
                var concatArray = children.concat(items);
                setChildren(concatArray);
            }
            fetchData();
            setRendered(true);
        }
    };

    useEffect(() => {
        handleListComment()
    }, [rendered, fin]);

    const handleAddComment = (e) => {
        e.preventDefault();
        const comment = e.target.elements.comment.value.trim();
        if (comment) {
          axios.post('../api/project/' + projectID + '/comment', {content: comment})
          .then(res => {
            toastr.success('Commentaire ajouté');
            setComment(res.data.comment);
            setChildren([]);
            setRendered(false);
            setFin(false);
        });
        e.target.elements.comment.value = "";
        }
      };

    return (
        <>
            <div className={classes.formAddComment}>
                <div className={classes.commentTile}>
                <ChatIcon className={classes.infoIcon} color="default" />
                <Typography variant={"h5"} gutterBottom>
                    <b>Commentaires</b>
                </Typography>
                </div>

                <form onSubmit={handleAddComment}>
                <div>
                    <FormControl className={classes.formControl}>
                    <TextareaAutosize
                        name="comment"
                        rowsMin={6}
                        rowsMax={10}
                        placeholder="Ajoutez un commentaire"
                        value={comment}
                        onChange={(e) => setComment(e.target.value)}
                    />
                    </FormControl>
                </div>
                <div>
                    <Button
                    type="submit"
                    variant="contained"
                    color="default"
                    className={classes.button}
                    endIcon={<AddCommentIcon />}
                    >
                    Poster
                    </Button>
                </div>
                </form>
                <Divider />
            </div>

            {children.length <= 0 ? <div style={CommentCardCSS}><Typography >Aucun commentaire</Typography></div> :
        children.map((children) => {
            return(<div style={CommentCardCSS}>
                <Card style={CommentColorCSS}>
                    <CardContent>
                        <div>
                            <Box style={CommentHeadCSS}>
                                <Box order={1}>
                                    <Typography gutterBottom>
                                        <b>{children.author_forename}</b>
                                    </Typography>
                                </Box>
                                <Box order={2} flexGrow={1} style={CommentDateCSS}>
                                    <Typography variant={"overline"} gutterBottom>
                                        le {GetDateInFormat(children.published_date)}
                                    </Typography>
                                </Box>
                            </Box>
                        </div>
    
                        <Divider />
    
                        <div>
                            <div style={ProjetCardContentCSS}>
                                <Typography>
                                    {children.content}
                                </Typography>
                            </div>
                        </div>
                    </CardContent>
                </Card>
            </div>)
        })}
        </>
        
    );
    
}

export default Comment;
