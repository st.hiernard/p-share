const projectListUpdaterInitialState = {
  ready: false,
  updater: {
    filiere: false,
    participants: false,
    status: false,
    sort: false,
    type: false
  }
}

const filiereInitialState = {
  key: undefined
};

const statusInitialState = {
  key: []
};

const typeInitialState = {
  key: []
};

const paricipantsInitialState = {
  min: 0,
  max: 20
};

export const projectListUpdaterReducer = (state = projectListUpdaterInitialState, action) => {
  const newState = {...state};

  if (action.type === 'UPDATER_READY') {
    newState.updater[action.value] = true;

    newState.ready = true;
    for (let k in newState.updater) {
      if (!newState.updater[k]) {
        newState.ready = false;
      }
    }
  }

  return newState;
};

export const filiereReducer = (state = filiereInitialState, action) => {
  const newState = {...state};

  if (action.type === 'SET_FILIERE_FILTERING') {
    newState.key = action.value.key;
  }

  return newState;
};

export const statusReducer = (state = statusInitialState, action) => {
  const newState = {...state};

  if (action.type === 'SET_STATUS_FILTERING') {
    newState.key = action.value.key;
  }

  return newState;
};

export const typeReducer = (state = typeInitialState, action) => {
  const newState = {...state};

  if (action.type === 'SET_TYPE_FILTERING') {
    newState.key = action.value.key;
  }

  return newState;
};

export const participantsReducer = (state = paricipantsInitialState, action) => {
  const newState = {...state};

  if (action.type === 'SET_PARTICIPANTS_FILTERING') {
    newState.min = action.value.min;
    newState.max = action.value.max;
  }

  return newState;
};

