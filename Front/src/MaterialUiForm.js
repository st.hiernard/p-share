import React from 'react';
import ReactFormValidation from "react-form-input-validation";
import 'date-fns';
import axios from "axios";
import { withStyles } from '@material-ui/styles';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Button from '@material-ui/core/Button';
import SendIcon from '@material-ui/icons/Send';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import Typography from '@material-ui/core/Typography';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import Divider from "@material-ui/core/Divider";
import { WithContext as InputTags } from 'react-tag-input'

import './react-tag-input.css'

const styles = theme => ({
  root: {
    '& > *': {
      margin: 15,
    },
  },
  formControl: {
    minWidth: 500,
  },
  formControl2: {
    minWidth: 250,
  },
  formControl3: {
    minWidth: 100,
  },
  textField: {
    minWidth: 500,
  },
  textField2: {
    minWidth: 50,
  },
  button: {
    minWidth: 250,
	marginRight: 4,
  },
  error: {
    color: 'red',
  },
  checkboxes: {
    display: 'flex',
  },
  duration: {
    '& .MuiTextField-root': {
      width: '20ch',
      margin: 5,
    },
  },
  select: {
    bottomMargin: 10,
  },
});

function GetDuration(year, month, day){
  let duration = 0;
  let d1 = year*31536000;
  let d2= month*2628000;
  let d3 = day*86400;
  duration = d1 + d2 + d3;
  return duration;
}

function GetDateIsoFormat(date){
  let dateIsoFormat = new Date(date).toISOString();
  return dateIsoFormat.substring(0, dateIsoFormat.length - 5);
}

class MaterialUiForm extends React.Component {
  constructor(props){
    super(props);
    this.initialState = {
      fields: {
        name: "",
        type: "",
        sector: "",
        short_description: "",
        theme: {
          tags: [],
          suggestions: []
        },
        duration: 0,
        details: "",
        goal: "",
        technos: {
          tags: [],
          suggestions: []
        },
        status: "",
        roles: "",
        begin_date: "",
        year: "0",
        month: "0",
        day: "0"
      },
      errors: {}
    };
    this.state = this.initialState;

    this.handleThemeDelete = this.handleThemeDelete.bind(this);
    this.handleThemeAddition = this.handleThemeAddition.bind(this);
    this.handleThemeDrag = this.handleThemeDrag.bind(this);
    this.handleTechnologiesDelete = this.handleTechnologiesDelete.bind(this);
    this.handleTechnologiesAddition = this.handleTechnologiesAddition.bind(this);
    this.handleTechnologiesDrag = this.handleTechnologiesDrag.bind(this);

    this.handleReset = () => {
      this.setState(() => this.initialState);
    }

    this.handleDuration = () => {
      this.setState(() => this.state.fields.duration = GetDuration(this.state.fields.year, this.state.fields.month, this.state.fields.day))
    }

    this.form = new ReactFormValidation(this, {locale: "fr"});
    this.form.useRules({ 
      name: "required",
      type: "required",
      short_description: "required",
      status: "required"
    });

    this.form.onformsubmit = (fields) => {
      const data = {
        name: fields.name, 
        type: fields.type,
        sector: fields.sector,
        short_description: fields.short_description,
        theme: fields.theme.tags.map(tag => tag.text),
        duration: fields.duration,
        details: fields.details,
        goal: fields.goal,
        technos: fields.technos.tags.map(tag => tag.text),
        status: fields.status,
        roles: fields.roles,
        begin_date: fields.begin_date ? GetDateIsoFormat(fields.begin_date) : ''
      };
      /* Remove empty fields */
      for (let k in data) {
        if (data[k] === '' || (Array.isArray(data[k])) && data[k].length === 0)
          delete data[k];
      }

      return axios.post('../api/project/new', data).then(({data}) => window.location.href = ('../ProjectPage/' + data.project.id));
    }
  }

  handleThemeDelete(i) {
    const { tags } = this.state.fields.theme;
    this.setState(state => ({
      fields: {
        ...state.fields,
        theme: {
          ...state.fields.theme,
          tags: tags.filter((tag, index) => index !== i)
        }
      }
    }));
  }

  handleThemeAddition(tag) {
    this.setState(state => ({
      fields: {
        ...state.fields,
        theme: {
          ...state.fields.theme,
          tags: [...state.fields.theme.tags, tag]
        }
      }
    }));
  }

  handleThemeDrag(tag, currPos, newPos) {
    const tags = [...this.state.fields.theme.tags];
    const newTags = tags.slice();

    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);

    this.setState(state => ({
      fields: {
        ...state.fields,
        theme: {
          ...state.fields.theme,
          tags: newTags
        }
      }
    }));
  }

  handleTechnologiesDelete(i) {
    const { tags } = this.state.fields.technos;
    this.setState(state => ({
      fields: {
        ...state.fields,
        technos: {
          ...state.fields.technos,
          tags: tags.filter((tag, index) => index !== i)
        }
      }
    }));
  }

  handleTechnologiesAddition(tag) {
    this.setState(state => ({
      fields: {
        ...state.fields,
        technos: {
          ...state.fields.technos,
          tags: [...state.fields.technos.tags, tag]
        }
      }
    }));
  }

  handleTechnologiesDrag(tag, currPos, newPos) {
    const tags = [...this.state.fields.technos.tags];
    const newTags = tags.slice();

    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);

    this.setState(state => ({
      fields: {
        ...state.fields,
        technos: {
          ...state.fields.technos,
          tags: newTags
        }
      }
    }));
  }

  render(){
    const {classes} = this.props;
    return (

      <form onSubmit={this.form.handleSubmit} onReset={this.handleReset} className={classes.root} noValidate autoComplete="off">
	  
	  <Typography variant="h4" style={{display: 'flex', alignItems: 'center'}}>
		<AddCircleIcon color="secondary" style={{fontSize: 40, marginRight: 10}}/>
		Ajouter un nouveau projet 
	  </Typography>
	  <Divider/>
	  
      <div>
        <TextField className={classes.textField} required
          id = "idProjectName"
          name="name"
          label="Nom du projet"
          onBlur={this.form.handleBlurEvent}
          onChange={this.form.handleChangeEvent}
          value={this.state.fields.name}
        />
        
      </div>
      <div>
      <label className={classes.error}>
          {this.state.errors.name
            ? this.state.errors.name
            : ""}
        </label>
      </div>
      <div>
      <FormControl className={classes.formControl}>
        <InputLabel id="idProjectType" required>Type de projet</InputLabel>
        <Select className={classes.select}
          labelId="typeLabel"
          id="idProjectType"
          name="type"
          onBlur={this.form.handleBlurEvent}
          onChange={this.form.handleChangeEvent}
          value={this.state.fields.type}
        >
          <MenuItem value="PA">Projet Association</MenuItem>
          <MenuItem value="PP">Projet Personnel</MenuItem>
          <MenuItem value="PR">PR</MenuItem>
          <MenuItem value="TX">TX</MenuItem>
          <MenuItem value="UV">UV</MenuItem>
        </Select>
      </FormControl>
      </div>
      <div>
      <label className={classes.error}>
          {this.state.errors.type
            ? this.state.errors.type
            : ""}
        </label>
      </div>
	  
	  <div>
      <FormControl className={classes.formControl}>
        <InputLabel id="idProjectType">Branche(s) visée(s)</InputLabel>
        <Select className={classes.select}
          labelId="SectorLabel"
          id="idProjectSector"
          name="sector"
          onBlur={this.form.handleBlurEvent}
          onChange={this.form.handleChangeEvent}
          value={this.state.fields.sector}
        >
          <MenuItem value="GB">GB</MenuItem>
          <MenuItem value="GI">GI</MenuItem>
          <MenuItem value="GP">GP</MenuItem>
          <MenuItem value="GU">GU</MenuItem>
          <MenuItem value="HuTech">HuTech</MenuItem>
		  <MenuItem value="IM">IM</MenuItem>
		  <MenuItem value="TC">TC</MenuItem>
        </Select>
      </FormControl>
      </div>
      <div>
      <label className={classes.error}>
          {this.state.errors.sector
            ? this.state.errors.sector
            : ""}
        </label>
      </div>
	  
      <div>
      <InputLabel required id="id_descriptionProject">Description</InputLabel>
          <FormControl className={classes.formControl}> 
              <TextareaAutosize 
                name="short_description"
                rowsMin={10} 
                rowsMax={10} 
                placeholder="Détaillez votre projet" 
                value={this.state.fields.short_description}
                onChange={this.form.handleChangeEvent}
                onBlur={this.form.handleBlurEvent}
              />
          </FormControl>
      </div>
      <div>
      <label className={classes.error}>
          {this.state.errors.short_description
            ? this.state.errors.short_description
            : ""}
        </label>
      </div>
      <div>
        <FormControl className={classes.formControl}>
          <InputTags
            name="theme"
            label="Thème"
            value={this.state.themeText}
            tags={this.state.fields.theme.tags}
            suggestions={this.state.fields.theme.suggestions}
            handleDelete={this.handleThemeDelete}
            handleAddition={this.handleThemeAddition}
            handleDrag={this.handleThemeDrag}
            delimiters={[9, 13, 188 /* tab, enter, comma */]}
            autofocus={false}
            placeholder="Thèmes * (séparés par des virgules)"
          />
        </FormControl>
      </div>
      <div>
        <label className={classes.error}>
          {this.state.errors.theme
            ? this.state.errors.theme
            : ""}
        </label>
      </div>
      <div>
        <InputLabel>Objectif(s)</InputLabel>
        <FormControl className={classes.formControl}> 
            <TextareaAutosize 
              name="goal"
              rowsMin={5} 
              rowsMax={5} 
              value={this.state.fields.goal}
              onChange={this.form.handleChangeEvent}
              onBlur={this.form.handleBlurEvent}
            />
        </FormControl>
      </div>
      <div>
        <InputLabel>Rôle(s) recherché(s)</InputLabel>
        <FormControl className={classes.formControl}> 
            <TextareaAutosize 
              name="roles"
              rowsMin={5} 
              rowsMax={5} 
              value={this.state.fields.roles}
              onChange={this.form.handleChangeEvent}
              onBlur={this.form.handleBlurEvent}
            />
        </FormControl>
      </div>
      <div>
      <FormControl className={classes.formControl}>
        <InputLabel required id="idProjectStatus">Etat du projet</InputLabel>
        <Select
          name="status"
          labelId="statusLabel"
          id="idProjectStatus"
          value={this.state.fields.status}
          onChange={this.form.handleChangeEvent}
          onBlur={this.form.handleBlurEvent}
        >
          <MenuItem value="idea">Stade initial d'idée</MenuItem>
          <MenuItem value="definition">Définition des besoins et des objectifs</MenuItem>
          <MenuItem value="specification">Spécifications du projet</MenuItem>
          <MenuItem value="conception">En phase de conception</MenuItem>
          <MenuItem value="development">En phase de développement</MenuItem>
          <MenuItem value="test">Mise en place de tests</MenuItem>
          <MenuItem value="production">Mise en production</MenuItem>
        </Select>
      </FormControl>
      </div>
      <div>
        <label className={classes.error}>
          {this.state.errors.status
            ? this.state.errors.status
            : ""}
        </label>
      </div>
      <div>

       <TextField className={classes.textField}
        type="date"
        margin="normal"
        id="date"
        label="Début du projet"
        name="begin_date"
        value={this.state.fields.begin_date}
        onChange={this.form.handleChangeEvent}
        onBlur={this.form.handleBlurEvent}
        InputLabelProps={{
          shrink: true,
        }}
      /> 
      </div>
      <div className={classes.duration}>
        <InputLabel>Durée estimée</InputLabel>
        <TextField 
          type="number"
          name="year"
          label="Année"
          fullWidth
          multiline={false}
          value={this.state.fields.year}
          onChange={this.form.handleChangeEvent}
          onBlur={this.form.handleBlurEvent} 
        />
        <TextField 
          type="number"
          name="month"
          label="Mois"
          fullWidth
          multiline={false}
          value={this.state.fields.month}
          onChange={this.form.handleChangeEvent}
          onBlur={this.form.handleBlurEvent} 
        />
        <TextField 
          type="number"
          name="day"
          label="Jour"
          fullWidth
          multiline={false}
          value={this.state.fields.day}
          onChange={this.form.handleChangeEvent}
          onBlur={this.form.handleBlurEvent} 
        />
      </div>
      <div>
        <InputTags required
          name="technos"
          label="Technologies"
          value={this.state.fields.technos}
          onChange={this.form.handleChangeEvent}
          onBlur={this.form.handleBlurEvent}
          tags={this.state.fields.technos.tags}
          suggestions={this.state.fields.technos.suggestions}
          handleDelete={this.handleTechnologiesDelete}
          handleAddition={this.handleTechnologiesAddition}
          handleDrag={this.handleTechnologiesDrag}
          delimiters={[9, 13, 188 /* tab, enter, comma */]}
          autofocus={false}
          placeholder="Technologies (séparés par des virgules)"
        />
      </div>
      <div>
          <InputLabel id="idMoreInformation">Plus d'informations sur le projet mené</InputLabel>
          <FormControl className={classes.formControl}> 
              <TextareaAutosize 
                rowsMin={7} 
                rowsMax={15} 
                name="details"
                placeholder="Remarques importantes, Informations supplémentaires à savoir..."
                value={this.state.fields.details}
                onChange={this.form.handleChangeEvent}
                onBlur={this.form.handleBlurEvent}
              />
          </FormControl>
      </div>
      
      <div>
      <Button
        type="submit"
        variant="contained"
        color="primary"
        className={classes.button}
        endIcon={<SendIcon/>}
		    onClick={this.handleDuration}
      >
        Envoyer
      </Button>
      <Button
        type="reset"
        variant="contained"
        color="default"
        className={classes.button}
        endIcon={<AutorenewIcon/>}
      >
        Effacer
      </Button>
      </div> 
    </form>
    );
  }
}


export default withStyles(styles)(MaterialUiForm);