const initialState = {
  loggedIn: false,
  displayName: 'Invité'
};

const reducer = (state = initialState, action) => {
  const newState = {...state};

  switch (action.type) {
    case 'LOG_IN':
      newState.loggedIn = true;
      newState.displayName = action.value;
      break;
    case 'LOG_OUT':
      newState.loggedIn = initialState.loggedIn;
      newState.displayName = initialState.displayName;
      break;
    default:
      // Not handled by this reducer
  }

  return newState;
};

export default reducer;
