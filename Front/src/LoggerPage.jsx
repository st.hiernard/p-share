import React from 'react';
import Typography from '@material-ui/core/Typography';

import { connect } from 'react-redux';

import axios from 'axios';

class Logger extends React.Component {
  componentDidMount() {
    // Check if user is logged in
    axios.get('/api/auth/login')
      .then((response) => {
        if (response.status === 200) {
          this.props.onLogIn(response.data.displayName);
        }
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 401) {
            document.location.href = error.response.data.redirect_url;
          }
        }
      });
  }

  render() {
    return (
      <Typography variant="body1" color="textPrimary" align="center">
        &#x270b; Vous devez être connecté au CAS de l'UTC pour utiliser P-Share &#x270b;<br />Vous allez être redirigé rapidement&hellip;
      </Typography>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    onLogIn: (displayName) => dispatch({ type: 'LOG_IN', value: displayName }),
    onLogOut: () => dispatch({ type: 'LOG_OUT' })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Logger);
