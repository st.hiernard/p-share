import React, { Component, useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import SortingAndFilters from './SortingAndFilters';
import ProjectTile, { ProjectTileExample } from './ProjectTile';
import AddIcon from '@material-ui/icons/Add';
import Container from '@material-ui/core/Container';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';
import Divider from "@material-ui/core/Divider";
import {
	BrowserRouter as Router,
	Link
} from "react-router-dom";
import axios from "axios";
import CircularProgress from '@material-ui/core/CircularProgress';


const HomePageCSS = {
	marginTop: '20px',
	marginBottom: '20px'
};

const AddProjectIconButtonCSS = {
	position: 'fixed',
	right: 10,
	bottom: 10,
	size: 'large'
}

const AddProjectIconCSS = {
	fontSize: 30
}

const TooltipCSS = {
	fontSize: 20
}

const ProjectTileSectionCSS = {
	marginTop: 10,
	marginBottom: 10
}

const CircularProgressCSS = {
	display: 'flex',
	justifyContent: 'center',
	width: '100%'
}

function HomePage(props) {
	return (
		<div>
			<Container style={HomePageCSS} maxWidth="md">
				<HomePageContent />
			</Container>
			<AddProjectButton />
		</div>
	);
}

function HomePageContent() {
	return (
		<div>
			<SortingAndFilters />
			<Divider light />
			<ProjectTileSection />
		</div>
	);
}

function AddProjectButton(props) {
	return (
		<Link to="/FormPage" >
			<Tooltip title={<Typography color="inherit" style={TooltipCSS}>Ajouter un projet</Typography>} interactive placement="left">
				<Fab style={AddProjectIconButtonCSS} color="secondary">
					<AddIcon style={AddProjectIconCSS} />
				</Fab>
			</Tooltip>
		</Link>
	);
}


function ProjectTileSection() {
	return (
			<div style={ProjectTileSectionCSS}>
				<Projects/>
			</div>
	);
}


function Projects(){
		const QUERY_LIMIT = 5;
		const [children, setChildren] = useState([]);
		const [rendered, setRendered] = useState(false);
		
		const [excluded, setExcluded] = useState([]);
		const [fin, setFin] = useState(false);
		const [pageBottom, setPageBottom] = useState(false);

		const sortBy = useSelector(state => state.sort);
		const filiere = useSelector(state => state.filiere);
		const status = useSelector(state => state.status);
		const type = useSelector(state => state.type);
		const participants = useSelector(state => state.participants);

		const updaterReady = useSelector(state => state.updater.ready)

		const getFilters = () => {
			return (
				{
					"filter": {
						"limit": QUERY_LIMIT,
						"excluding": excluded,
						"sort": { ...sortBy },
						"sector": filiere.key,
						"status": {
							"including": status.key
						},
						"type": {
							"including": type.key
						},
						"member": {
							"min": participants.min,
							"max": participants.max
						}
					}
				}
			)
		}

		useEffect(() => {
			setChildren([]);
			setExcluded([]);
			setRendered(false);
			setFin(false);
		}, [sortBy, filiere, status, type, participants]);

		useEffect(() => {
			if(updaterReady && !rendered && !fin){
				axios('api/project/list', { params: getFilters() })
				.then(result => {
					let newChildren = [];
					let newExcluded = [];
					result.data.forEach(element => {
						newChildren.push(element);
						newExcluded.push(element.id);
					});

					if (result.data.length < QUERY_LIMIT)
						setFin(true);

					setChildren(children.concat(newChildren));
					setExcluded(excluded.concat(newExcluded));
				}).catch(error => console.log(error));

				setRendered(true);
			}
		}, [children, excluded, updaterReady, rendered, fin]);

		//On rerender si on atteint le bas de la bage
		const handleScroll = () => {
			if(document.documentElement.scrollHeight - document.documentElement.scrollTop === document.documentElement.clientHeight){
				setRendered(false);
				setPageBottom(true);
			}else{
				setPageBottom(false);
			}
		}

    // Register event listener only once, and unregister on component unmount
    useEffect(() => {
      document.addEventListener('scroll', handleScroll);
      return () => {
        document.removeEventListener('scroll', handleScroll);
      };
    }, []);


		return(<div>{
			children.length <= 0 ? null :
				children.map((val, index) => {
					return(
							<ProjectTile key={val.id} projectID={val.id} date={val.publication} tags={val.tags} projectTitle={val.name} projectDescription={val.description} delai={val.duration} nbParticipants={val.participants} vote={val.votes} currentVote={val.currentVote} isSubscribed={val.isSubscribed} />
					);
				})
		}
		{fin?null:<Container style={CircularProgressCSS}><CircularProgress color="primary"/></Container>}
		</div>);

}


export default HomePage;
