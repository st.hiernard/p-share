import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from '@material-ui/styles';
import axios from "axios";
import toastr from "toastr";

const classes = theme => ({
    project: {
		width: '100%',
		height: '100%',
        borderRadius: 5,
        paddingRight: 5,
    },
    contentTile: {
        display: 'flex',
        flexDirection : 'row',
    },
    voteSystem: {
        marginRight: 10,
	    display: 'flex',
	    flexDirection : 'column',
	    alignItems: 'center',
	    justifyContent: 'center',
	    alignContent: 'center',
    },
    projectColor: {
        backgroundColor: "#f7f975",
        marginRight: 10,
    },
    projectCardContent: {
        marginTop: 20,
    },
    projectInfo: {
        display: 'flex',
	    flexDirection : 'row',
        alignItems: 'center',  
    },
    projectDescription: {
        textAlign: 'justify',
    },
    projectDetails: {
        textAlign: 'justify',
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20,
        marginTop: 10,
    },
});

class PresentationProject extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vote : this.props.votes,
            cssUpvote: this.props.currentVote === 1 ? { color: '#00df00' } : {},
            cssDownvote: this.props.currentVote === -1 ? { color: '#df0000' } : {},
            currentVote: this.props.currentVote
        }
    }

    upvote = () => {
        if (this.state.currentVote !== 0) {
            // Reset vote
            axios.post('/api/project/' + this.props.projectID + '/resetvote').then(res => {
                toastr.success('Vote annulé');
                this.setState({
                    vote: res.data.vote,
                    cssUpvote: {},
                    cssDownvote: {},
                    currentVote: 0
                });
            });
        } else {
            // Send upvote
            axios.post('/api/project/' + this.props.projectID + '/upvote').then(res => {
                toastr.success('Vote enregistré');
                this.setState({
                    vote: res.data.vote,
                    cssUpvote: { color: '#00df00' },
                    cssDownvote: {},
                    currentVote: 1
                });
            });
        }
    };

    downvote = () => {
        if (this.state.currentVote !== 0) {
            // Reset vote
            axios.post('/api/project/' + this.props.projectID + '/resetvote').then(res => {
                toastr.success('Vote annulé');
                this.setState({
                    vote: res.data.vote,
                    cssUpvote: {},
                    cssDownvote: {},
                    currentVote: 0
                });
            });
        } else {
            // Send downvote
            axios.post('/api/project/' + this.props.projectID + '/downvote').then(res => {
                toastr.success('Vote enregistré');
                this.setState({
                    vote: res.data.vote,
                    cssUpvote: {},
                    cssDownvote: { color: '#df0000' },
                    currentVote: -1
                });
            });
        }
    };

    render(){
        const {classes} = this.props;
        return (
            <div className={classes.project}>
            <Card className={classes.projectColor}>
                <CardContent>
                    <div className= {classes.contentTile}>
                        <Box  className={classes.voteSystem}>
                            <Box order={1}>
                                <IconButton aria-label="Upvote" size="small" onClick={this.upvote} style={this.state.cssUpvote}>
                                    <ArrowUpwardIcon/>
                                </IconButton>
                            </Box>
                            <Box order={2}>
                                <Typography variant={"subtitle2"} >
                                    {this.state.vote}
                                </Typography>
                            </Box>
                            <Box order={3}>
                                <IconButton aria-label="Downvote" size="small" onClick={this.downvote} style={this.state.cssDownvote}>
                                    <ArrowDownwardIcon/>
                                </IconButton>
                            </Box>
                        </Box>
                        <div className={classes.projectCardContent}>
                            <Typography variant={"h5"} gutterBottom>
                            <b>{this.props.name}</b>
                            </Typography>
                            <Typography className={classes.projectDescription}>
                                {this.props.description}
                            </Typography>
                            
                        </div>
                        
                        

                    </div>
                </CardContent>
                <div className={classes.projectDetails}>
                    <Divider/>
                    <Typography variant="h6">Objectif</Typography> 
                    <Typography>
                        {this.props.goal}
                    </Typography>
                    <Divider/>
                    <Typography variant="h6">Détails</Typography> 
                    <Typography>
                        {this.props.details}
                    </Typography>
                </div>
            </Card>
        </div>
        );
    }
}

export default withStyles(classes)(PresentationProject); 