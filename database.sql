
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

CREATE DATABASE `pshare` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pshare`;

--
-- Structure for table: comment
--
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `posted_at` datetime NOT NULL,
  `content` text NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fkey_ynzsziufdg` (`user_id`),
  KEY `fkey_fsnwlceyfy` (`project_id`),
  KEY `fkey_oildgijbum` (`parent_id`),
  CONSTRAINT `fkey_fsnwlceyfy` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fkey_oildgijbum` FOREIGN KEY (`parent_id`) REFERENCES `comment` (`id`) ON DELETE SET NULL,
  CONSTRAINT `fkey_ynzsziufdg` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Structure for table: participant
--
CREATE TABLE `participant` (
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `role` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`project_id`),
  KEY `fkey_opekicfjha` (`project_id`),
  KEY `fkey_tobutjnfmy` (`user_id`),
  CONSTRAINT `fkey_opekicfjha` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fkey_tobutjnfmy` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Structure for table: project
--
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `reported` tinyint(1) DEFAULT 0,
  `goal` text DEFAULT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `publishedAt` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `technos` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `sector` enum('GI','IM','GSU','GB','GP') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fkey_ztwwsregjr` (`user_id`),
  CONSTRAINT `fkey_ztwwsregjr` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Structure for table: subscription
--
CREATE TABLE `subscription` (
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`project_id`),
  KEY `fkey_odqyupfjha` (`project_id`),
  KEY `fkey_tparzynfmy` (`user_id`),
  CONSTRAINT `fkey_odqyupfjha` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fkey_tparzynfmy` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Structure for table: user
--
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


--
-- Structure for table: vote
--
CREATE TABLE `vote` (
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `value` int(1) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`user_id`,`project_id`),
  KEY `fkey_eblqmcoxzy` (`project_id`),
  KEY `fkey_sxsnyeratc` (`user_id`),
  CONSTRAINT `fkey_eblqmcoxzy` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fkey_sxsnyeratc` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Data for table: comment
--
LOCK TABLES `comment` WRITE;
ALTER TABLE `comment` DISABLE KEYS;

-- Table contains no data

ALTER TABLE `comment` ENABLE KEYS;
UNLOCK TABLES;
COMMIT;

--
-- Data for table: participant
--
LOCK TABLES `participant` WRITE;
ALTER TABLE `participant` DISABLE KEYS;

-- Table contains no data

ALTER TABLE `participant` ENABLE KEYS;
UNLOCK TABLES;
COMMIT;

--
-- Data for table: project
--
LOCK TABLES `project` WRITE;
ALTER TABLE `project` DISABLE KEYS;

-- Table contains no data

ALTER TABLE `project` ENABLE KEYS;
UNLOCK TABLES;
COMMIT;

--
-- Data for table: subscription
--
LOCK TABLES `subscription` WRITE;
ALTER TABLE `subscription` DISABLE KEYS;

-- Table contains no data

ALTER TABLE `subscription` ENABLE KEYS;
UNLOCK TABLES;
COMMIT;

--
-- Data for table: user
--
LOCK TABLES `user` WRITE;
ALTER TABLE `user` DISABLE KEYS;

INSERT INTO `user` (`id`,`login`,`firstname`,`lastname`,`photo`) VALUES (1,'ewauquie','Elouan','Wauquier','none');

ALTER TABLE `user` ENABLE KEYS;
UNLOCK TABLES;
COMMIT;

--
-- Data for table: vote
--
LOCK TABLES `vote` WRITE;
ALTER TABLE `vote` DISABLE KEYS;

-- Table contains no data

ALTER TABLE `vote` ENABLE KEYS;
UNLOCK TABLES;
COMMIT;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;