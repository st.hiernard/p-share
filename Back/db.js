const mysql = require('mysql');

// Get database connection credentials
const { db } = require('./config');

// Create a MySQL pool
const pool = mysql.createPool(db);

// Export the pool
module.exports = pool;
