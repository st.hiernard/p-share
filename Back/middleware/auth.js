const cas = {
  BASE_URL: 'https://cas.utc.fr/cas',
  LOGIN_ROUTE: '/login?service=',
  VALIDATE_ROUTE: '/serviceValidate?service='
};

function getHostname(url) {
  let [scheme, urlNoScheme] = url.split('://', 2);
  let authority = urlNoScheme.split('/', 1)[0];
  return scheme + '://' + authority;
}

/** Check if user is logged in */
module.exports = (req, res, next) => {
  if (!req.session.user) {
    res.status(401).json({
      redirect_url: cas.BASE_URL + cas.LOGIN_ROUTE +
      getHostname(req.headers.referer) + '/api/auth/login'
    });
  } else {
    next();
  }
};
