module.exports = (req, res, next) => {
  try {
    for (let k in req.query) {
      req.query[k] = JSON.parse(req.query[k]);
    }
  } catch (e) {
    next(e);
  }
  next();
};
