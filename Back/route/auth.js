const express = require('express');
const https = require('https');

const { cas } = require('./conf');
const pool = require('../db');

let router = express.Router();

router.get('/login', (req, res, next) => {
  /* Check user authentication status in session */

  // User is already logged in
  if (typeof req.session.user !== 'undefined' && req.session.user) {
    res.status(200).json({
      displayName: req.session.user.displayName
    });
    return;
  }

  /* Validate ticket issued by the CAS server */

  // No ticket passed in query parameters
  if (typeof req.query.ticket === 'undefined' || !req.query.ticket) {
    res.status(401);
    res.json(
      {
        redirect_url: cas.BASE_URL + cas.LOGIN_ROUTE +
        req.protocol + '://' + req.hostname + ':3000' +
        req.baseUrl + '/login'
      }
    );
    return;
  }

  // Check ticket
  https.get(cas.BASE_URL + cas.VALIDATE_ROUTE +
    req.protocol + '://' + req.hostname + ':3000' +
    req.baseUrl + '/login&format=JSON&ticket=' + req.query.ticket,
    (response) => {
    let data = [];

    response.on('data', (fragments) => {
      data.push(fragments);
    });

    response.on('end', () => {
      let body = Buffer.concat(data);

      if (body.length == 0) {
        res.sendStatus(500);
        return;
      }

      try {
        result = JSON.parse(body.toString());
      } catch (e) {
        return next(e);
      }

      // Invalid ticket
      if (typeof result.serviceResponse.authenticationSuccess === 'undefined' || !result.serviceResponse.authenticationSuccess) {
        console.log(result);
        res.sendStatus(401);
        return;
      }

      // Set session
      req.session.user = {
        login: result.serviceResponse.authenticationSuccess.user,
        email: result.serviceResponse.authenticationSuccess.attributes.mail[0],
        displayName: result.serviceResponse.authenticationSuccess.attributes.displayName[0],
        id: undefined
      };
      console.log('Authenticated: ' + req.session.user.login);

      // Get user id
      let query = `SELECT id
                     FROM user
                     WHERE login = ?
                       AND firstname = ?
                       AND lastname = ?
                       AND email = ?;`;
/*      let query = `INSERT INTO user (login, firstname, lastname, email)
                     SELECT tmp.*
                       FROM (
                         SELECT ? login, ? firstname, ? lastname, ? email
                       ) tmp
                         LEFT JOIN user
                           ON (
                             user.login = tmp.login
                             AND user.firstname = tmp.firstname
                             AND user.lastname = tmp.lastname
                             AND user.email = tmp.email
                           )
                       WHERE user.id IS NULL;`;*/
      query = query.replace(/\n/g, ' ').replace(/ +(?= )/g, '');
      let parameters = [
        req.session.user.login,
        result.serviceResponse.authenticationSuccess.attributes.givenName[0],
        result.serviceResponse.authenticationSuccess.attributes.sn[0],
        req.session.user.email
      ];

      pool.query(query, parameters, (err, result, fields) => {
        if (err)
          return next(err);

        if (result.length === 1) {
          req.session.user.id = result[0].id;

          res.redirect('/');
        } else {
          // Insert new user
          let query = `INSERT INTO user (login, firstname, lastname, email)
                         VALUES (?, ?, ?, ?);`;
          query = query.replace(/\n/g, ' ').replace(/ +(?= )/g, '');

          pool.query(query, parameters, (err, result, fields) => {
            if (err)
              return next(err);

            req.session.user.id = result.insertId;

            res.redirect('/');
          });
        }
      });
    });

    response.on('error', (error) => {
      next(error);
    });
  });
});

router.get('/logout', (req, res) => {
  delete req.session.user;
  res.sendStatus(200);
});

module.exports = router;
