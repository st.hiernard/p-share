const express = require('express');

const conf = require('./conf');

let router = express.Router();

router.get('/stat/:id?', (req, res, next) => {
  /* Fetch limited user data
   *
   * Response:
   * {
   *   login: string
   *   forename: string
   *   surname: string
   *   picture: url
   * }
   */
   next();
});

router.get('/:id?', (req, res, next) => {
  /* Fetch detailed user data
   *
   * Response:
   * {
   *   login: string
   *   forename: string
   *   surname: string
   *   picture: url
   * }
   *
   */
   next();
});

module.exports = router;
