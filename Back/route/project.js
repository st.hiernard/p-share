const express = require('express');
const util = require('util');
const { validateRoute } = require('express-ajv-middleware');
const queryStringParser = require('../middleware/query-string-parser');
const { paramValidator, paramEmailValidator } = require('../middleware/param-validator');
const { db } = require('./conf');
const filters = require('./filter');
const patterns = require ('./pattern');
const pool = require('../db');

let router = express.Router();

router.param('id', paramValidator);
router.param('parent', paramValidator);
router.param('email', paramEmailValidator);

const tagRecordSet = records => {
  /* Take a set of the same project returned by a SQL query
   * and tag it appropriately
   */
  base = Object.assign({}, records[0]);
  base.tags = [];
  base.tags.push({ category: 'sector', value: base.sector });
  delete base.theme;
  delete base.sector;
  delete base.technos;

  for (record of records)
    base.tags.push({ category: 'theme', value: record.theme });
  for (record of records)
    base.tags.push({ category: 'technos', value: record.technology })

  // Remove duplicate entries and null
  base.tags = base.tags.filter((tag, index, self) =>
    index === self.findIndex((t) => (
      t.category === tag.category && t.value === tag.value && tag.value !== null
    ))
  );

  return base;
}

const listFilterValidator = validateRoute({
  query: {
    type: 'object',
    properties: {
      filter: {
        type: 'object',
        properties: {
          member: {
            type: 'object',
            properties: {
              min: { type: 'integer', minimum: 0 },
              max: { type: 'integer', minimum: 0 },
              including_any: { // must have any of the user IDs
                type: 'array',
                items: { type: 'integer', minimum: 0 }
              },
              including_all: { // must have all of the user IDs
                type: 'array',
                items: { type: 'integer', minimum: 0 }
              },
              excluding_any: { // must not have any of the user IDs
                type: 'array',
                items: { type: 'integer', minimum: 0 }
              },
              excluding_all: { // must not have all of the user IDs
                type: 'array',
                items: { type: 'integer', minimum: 0 }
              }
            }
          },
          leader: {
            type: 'object',
            properties: {
              min: { type: 'integer', minimum: 0 },
              max: { type: 'integer', minimum: 0 },
              including_any: { // must have any of the user IDs
                type: 'array',
                items: { type: 'integer', minimum: 0 }
              },
              including_all: { // must have all of the user IDs
                type: 'array',
                items: { type: 'integer', minimum: 0 }
              },
              excluding_any: { // must not have any of the user IDs
                type: 'array',
                items: { type: 'integer', minimum: 0 }
              },
              excluding_all: { // must not have all of the user IDs
                type: 'array',
                items: { type: 'integer', minimum: 0 }
              }
            }
          },
          author: {
            type: 'object',
            properties: {
              including: { // must be authored by any of the user IDs
                type: 'array',
                items: { type: 'integer', minimum: 0 }
              },
              excluding: { // must not be authored by any of the user IDs
                type: 'array',
                items: { type: 'integer', minimum: 0 }
              },
            }
          },
          follower: {
            type: 'object',
            properties: {
              min: { type: 'integer', minimum: 0 },
              max: { type: 'integer', minimum: 0 }
            }
          },
          vote: {
            type: 'object',
            properties: {
              min: { type: 'integer' },
              max: { type: 'integer' }
            }
          },
          /* TODO: sector, theme: allow for multiple values */
          sector: { type: 'string', pattern: patterns.sector },
          theme: {
            type: 'object',
            properties: {
              including_any: { // must have any of the user IDs
                type: 'array',
                items: { type: 'integer', minimum: 0 }
              },
              including_all: { // must have all of the user IDs
                type: 'array',
                items: { type: 'integer', minimum: 0 }
              },
              excluding_any: { // must not have any of the user IDs
                type: 'array',
                items: { type: 'integer', minimum: 0 }
              },
              excluding_all: { // must not have all of the user IDs
                type: 'array',
                items: { type: 'integer', minimum: 0 }
              }
            }
          },
          time: {
            type: 'object',
            properties: {
              published_after: { type: 'string', pattern: patterns.datetime },
              published_before: { type: 'string', pattern: patterns.datetime },
              beginning_after: { type: 'string', pattern: patterns.datetime },
              beginning_before: { type: 'string', pattern: patterns.datetime },
              duration_min: { type: 'integer', minimum: 0 },
              duration_max: { type: 'integer', minimum: 0 },
              ending_after: { type: 'string', pattern: patterns.datetime },
              ending_before: { type: 'string', pattern: patterns.datetime }
            }
          },
          status: {
            type: 'object',
            properties: {
              including: {
                type: 'array',
                items: { type: 'string', pattern: patterns.status }
              },
              excluding: {
                type: 'array',
                items: { type: 'string', pattern: patterns.status }
              },
            }
          },
          type: {
            type: 'object',
            properties: {
              including: {
                type: 'array',
                items: { type: 'string', pattern: patterns.projectType }
              },
              excluding: {
                type: 'array',
                items: { type: 'string', pattern: patterns.projectType }
              },
            }
          },
          limit: { type: 'integer', minimum: 1, maximum: db.QUERY_LIMIT_MAX },
          excluding: { // project IDs, typically already loaded projects
            type: 'array',
            items: { type: 'integer', minimum: 0 }
          },
          sort: {
            type: 'object',
            properties: {
              key: { type: 'string', pattern: patterns.sort_key },
              order: { type: 'string', pattern: patterns.sort_order }
            },
            required: ['key', 'order']
          },
          report: { // restricted to admins, TODO
            type: 'object',
            properties: {
              min: { type: 'integer', minimum: 0 },
              max: { type: 'integer', minimum: 0 }
            }
          },
          is_banned: { // restricted to admins, TODO
            type: 'boolean'
          }
        }
      }
    }
  }
});
router.get('/list',
  queryStringParser,
  listFilterValidator,
  (req, res, next) => {
  /* Fetch a list of projects with limited project data
   *
   * Response:
   * [
   *   {
   *     id: integer >= 0
   *     name: string
   *     tags: [{category: enum_string, value: string}]
   *     votes: integer
   *     participants: integer
   *     duration: integer, in seconds
   *     publication: date-time
   *     description: string
   *     reports: integer, restricted to admins TODO
   *   }
   * ]
   */

  // Get filter parameter
  let filter = req.query.filter || {};

  // Fill filter defaults
  filter.limit = filter.limit || db.QUERY_LIMIT_MAX;


  // Parse filter
  let constraints = [];
  let parameters = [];

  /* Simple filtering */
  if (filter.time) {
    if (filter.time.published_after) {
      constraints.push(`publishedAt >= ?`);
      parameters.push(filter.time.published_after);
    }
    if (filter.time.published_before) {
      constraints.push(`publishedAt <= ?`);
      parameters.push(filter.time.published_before);
    }
    if (filter.time.beginning_after) {
      constraints.push(`begin_date >= ?`);
      parameters.push(filter.time.beginning_after);
    }
    if (filter.time.beginning_before) {
      constraints.push(`begin_date <= ?`);
      parameters.push(filter.time.beginning_before);
    }
    if (filter.time.duration_min) {
      constraints.push(`duration >= ?`);
      parameters.push(filter.time.duration_min);
    }
    if (filter.time.duration_max) {
      constraints.push(`duration <= ?`);
      parameters.push(filter.time.duration_max);
    }
    if (filter.time.ending_after) {
      constraints.push(`ADDTIME(begin_date, duration) >= ?`);
      parameters.push(filter.time.ending_after);
    }
    if (filter.time.ending_before) {
      constraints.push(`ADDTIME(begin_date, duration) <= ?`);
      parameters.push(filter.time.ending_before);
    }
  }

  if (filter.excluding && filter.excluding.length) {
    constraints.push(`id NOT IN (?)`);
    parameters.push(filter.excluding);
  }

  if (filter.sector) {
    constraints.push(`sector = ?`);
    parameters.push(filter.sector);
  }

  if (constraints.length) {
    // Update constraints
    let query = `(SELECT id
                    FROM project
                    WHERE ${constraints.join(' AND ')})`;
    constraints = [`project.id IN ${query}`];
  }


  /* Complex filtering */
  if (filter.member) {
    if (filter.member.min !== undefined || filter.member.max !== undefined)
      filters.countMinMax(constraints, parameters, filter.member, {
        match: 'user_id',
        table: 'project',
        secondaryTable: 'participant'
      });
    if (filter.member.including_any && filter.member.including_any.length ||
        filter.member.excluding_any && filter.member.excluding_any.length)
      filters.matchAny(constraints, parameters, filter.member, {
        match: 'user_id',
        table: 'project',
        secondaryTable: 'participant'
      });
    if (filter.member.including_all && filter.member.including_all.length ||
        filter.member.excluding_all && filter.member.excluding_all.length)
      filters.matchAll(constraints, parameters, filter.member, {
        match: 'user_id',
        table: 'project',
        secondaryTable: 'participant'
      });
  }

  if (filter.leader) {
    if (filter.leader.min !== undefined || filter.leader.max !== undefined)
      filters.countMinMax(constraints, parameters, filter.leader, {
        match: 'user_id',
        table: 'project',
        secondaryTable: 'participant',
        role: 'leader'
      });
    if (filter.leader.including_any && filter.leader.including_any.length ||
        filter.leader.excluding_any && filter.leader.excluding_any.length)
      filters.matchAny(constraints, parameters, filter.leader, {
        match: 'user_id',
        table: 'project',
        secondaryTable: 'participant',
        role: 'leader'
      });
    if (filter.leader.including_all && filter.leader.including_all.length ||
        filter.leader.excluding_all && filter.leader.excluding_all.length)
      filters.matchAll(constraints, parameters, filter.leader, {
        match: 'user_id',
        table: 'project',
        secondaryTable: 'participant',
        role: 'leader'
      });
  }

  if (filter.author &&
       (filter.author.including && filter.author.including.length ||
        filter.author.excluding && filter.author.excluding.length)) {
    filter.author.including_any = filter.author.including;
    filter.author.excluding_any = filter.author.excluding;
    filters.matchAny(constraints, parameters, filter.author, {
      match: 'user_id',
      table: 'project',
      secondaryTable: 'participant',
      role: 'author'
    });
  }

  if (filter.follower && (filter.follower.min || filter.follower.max))
    filters.countMinMax(constraints, parameters, filter.follower, {
      match: 'user_id',
      table: 'project',
      secondaryTable: 'subscription'
    });

  if (filter.vote !== undefined && (filter.vote.min || filter.vote.max))
    filters.sumMinMax(constraints, parameters, filter.vote, {
      match: 'value',
      table: 'project',
      secondaryTable: 'vote'
    });

  if (filter.status &&
       (filter.status.including && filter.status.including.length ||
        filter.status.excluding && filter.status.excluding.length)) {
      filter.status.including_any = filter.status.including;
      filter.status.excluding_any = filter.status.excluding;
    filters.matchAny(constraints, parameters, filter.status, {
      match: 'status',
      table: 'project'
    });
  }

  if (filter.type &&
       (filter.type.including && filter.type.including.length ||
        filter.type.excluding && filter.type.excluding.length)) {
      filter.type.including_any = filter.type.including;
      filter.type.excluding_any = filter.type.excluding;
    filters.matchAny(constraints, parameters, filter.type, {
      match: 'type',
      table: 'project'
    });
  }

  if (filter.theme) {
    if (filter.theme.including_any && filter.theme.including_any.length ||
        filter.theme.excluding_any && filter.theme.excluding_any.length)
      filters.matchAny(constraints, parameters, filter.theme, {
        match: 'theme_id',
        table: 'project',
        secondaryTable: 'project_theme'
      });
    if (filter.theme.including_all && filter.theme.including_all.length ||
        filter.theme.excluding_all && filter.theme.excluding_all.length)
      filters.matchAll(constraints, parameters, filter.theme, {
        match: 'theme_id',
        table: 'project',
        secondaryTable: 'project_theme'
      });
  }

  let ordering = 'votes DESC, publication DESC';
  if (filter.sort) {
    if (filter.sort.key === 'vote')
      ordering = 'votes {}, publication DESC';
    else
      ordering = 'publication {}, votes DESC';

    if (filter.sort.order === 'descending')
      ordering = ordering.replace('{}', 'DESC');
    else
      ordering = ordering.replace('{}', 'ASC');
  }

  let query = `SELECT project.*,
                      theme.name AS theme,
                      technology.name AS technology,
                      COALESCE(vote.value, 0) AS currentVote,
                      NOT(ISNULL(subscription.user_id)) AS isSubscribed
                 FROM (
                   SELECT project.id,
                          project.name,
                          project.sector,
                          COALESCE(SUM(vote.value), 0) AS votes,
                          project.participants,
                          project.duration,
                          project.publishedAt AS publication,
                          project.description,
                          project.status,
                          project.type
                     FROM (
                       SELECT project.id,
                              project.name,
                              project.sector,
                              COUNT(participant.user_id) AS participants,
                              project.duration,
                              project.publishedAt,
                              project.short_description AS description,
                              project.status,
                              project.type
                         FROM project
                           LEFT JOIN participant
                           ON project.id = participant.project_id`;
  if (constraints.length)
    query +=           ` WHERE ${constraints.join(' AND ')}`;
  query +=             ` GROUP BY project.id
                       ) project
                       LEFT JOIN vote
                       ON project.id = vote.project_id
                     GROUP BY project.id
                     ORDER BY ${ordering}
                     LIMIT ?
                   ) project
                   LEFT JOIN vote
                   ON project.id = vote.project_id AND vote.user_id = ?
                   LEFT JOIN subscription
                   ON project.id = subscription.project_id AND subscription.user_id = ?
                   INNER JOIN project_theme
                   ON project.id = project_theme.project_id
                   INNER JOIN theme
                   ON theme.id = project_theme.theme_id
                   LEFT JOIN project_technology
                   ON project.id = project_technology.project_id
                   LEFT JOIN technology
                   ON technology.id = project_technology.technology_id;`;
  parameters.push(filter.limit);
  parameters.push(req.session.user.id);
  parameters.push(req.session.user.id);
  query = query.replace(/\n/g, ' ').replace(/ +(?= )/g, '');

  pool.query(query, parameters, (err, results, fields) => {
    if (err)
      return next(err);

    projects = [];
    while (results.length) {
      projects.push(tagRecordSet(results.filter(row => row.id === results[0].id)));
      results = results.filter(row => row.id !== results[0].id);
    }

    res.json(projects);
  });
});

router.get('/:id', (req, res, next) => {
  /* Fetch the details of a project
   *
   * Report status is only sent to admins
   * Banned projects can only be seen by admins
   *
   * Response:
   * {
   *   name: string
   *   tags: [{category: enum_string, value: string}]
   *   roles: string
   *   votes: integer
   *   participants: integer
   *   duration: integer
   *   description: string
   *   details: string
   *   publication_date: ISO datetime (YYYY-MM-DDTHH:MM:SSZ)
   *   goal: string
   *   status: enum_string
   *   is_subscribed: boolean
   *   author_email: string
   *   author_displayname: string
   *   reports: integer, restricted to admins
   *   is_banned: boolean, restricted to admins
   * }
   */
  let query = `SELECT project.*,
                      technology.name AS technology,
                      theme.name AS theme
                 FROM (
                   SELECT project.id,
                          project.name,
                          project.sector,
                          project.roles,
                          project.votes,
                          COUNT(participant.user_id) AS participants,
                          project.duration,
                          project.description,
                          project.details,
                          project.publication_date,
                          project.goal,
                          project.status,
                          COALESCE(vote.value, 0) AS current_vote,
                          NOT(ISNULL(subscription.user_id)) AS is_subscribed,
                          user.email AS author_email,
                          CONCAT(user.firstname, ' ', user.lastname) AS author_display_name
                     FROM (
                       SELECT project.id,
                              project.name,
                              project.sector,
                              project.technos,
                              project.roles,
                              COALESCE(SUM(vote.value), 0) AS votes,
                              project.duration,
                              project.short_description AS description,
                              project.details,
                              project.publishedAt AS publication_date,
                              project.goal,
                              project.status,
                              project.user_id
                         FROM project
                           LEFT JOIN vote
                           ON vote.project_id = project.id
                         WHERE project.id = ?
                         GROUP BY project.id
                       ) project
                         LEFT JOIN participant
                         ON project.id = participant.project_id
                         LEFT JOIN vote
                         ON project.id = vote.project_id AND vote.user_id = ?
                         LEFT JOIN subscription
                         ON project.id = subscription.project_id AND subscription.user_id = ?
                         INNER JOIN user
                         ON project.user_id = user.id
                       GROUP BY project.id
                     ) project
                       INNER JOIN project_theme
                       ON project.id = project_theme.project_id
                       INNER JOIN theme
                       ON theme.id = project_theme.theme_id
                       LEFT JOIN project_technology
                       ON project.id = project_technology.project_id
                       LEFT JOIN technology
                       ON technology.id = project_technology.technology_id;`;
  query = query.replace(/\n/g, ' ').replace(/ +(?= )/g, '');

  let parameters = [res.locals.id, req.session.user.id, req.session.user.id];

  pool.query(query, parameters, (err, results, fields) => {
    if (err)
      return next(err);
    if (results.length) {
      let project = tagRecordSet(results);
      res.json(project);
    } else {
      res.sendStatus(404);
    }
  });
});

const participantsFilterValidator = validateRoute({
  query: {
    type: 'object',
    properties: {
      filter: {
        type: 'object',
        properties: {
          role: {
            type: 'object',
            properties: {
              including: { // must have any of the roles
                type: 'array',
                items: { type: 'string', pattern: patterns.role }
              },
              excluding: { // must not have any of the roles
                type: 'array',
                items: { type: 'string', pattern: patterns.role }
              }
            }
          },
          limit: { type: 'integer', minimum: 1, maximum: db.QUERY_LIMIT_MAX },
          excluding: { // user IDs, typically already loaded participants
            type: 'array',
            items: { type: 'integer', minimum: 0 }
          }
        }
      }
    }
  }
});
router.get('/:id/participants',
  queryStringParser,
  participantsFilterValidator,
  (req, res, next) => {
  /* Fetch a list of participants to a project
   *
   * Response:
   * [
   *   {
   *     id: integer < 0
   *     forename: string
   *     surname: string
   *     role: enum_string
   *     email: string
   *     picture: url
   *   }
   * ]
   */

  // Get filter parameter
  let filter = req.query.filter || {};

  // Fill filter defaults
  filter.limit = filter.limit || db.QUERY_LIMIT_MAX;


  // Parse filter
  let constraints = ['participant.project_id = ?'];
  let parameters = [res.locals.id];

  if (filter.role) {
    if (filter.role.including && filter.role.including.length) {
      constraints.push('participant.role IN (?)');
      parameters.push(filter.role.including);
    }
    if (filter.role.excluding && filter.role.excluding.length) {
      constraints.push('participant.role NOT IN (?)');
      parameters.push(filter.role.excluding);
    }
  }

  if (filter.excluding && filter.excluding.length) {
    constraints.push(`id NOT IN (?)`);
    parameters.push(filter.excluding);
  }

  query = `SELECT user.id,
                  user.firstname AS forename,
                  user.lastname AS surname,
                  participant.role,
                  user.email,
                  user.photo AS picture
             FROM user
               INNER JOIN participant
               ON user.id = participant.user_id
             WHERE ${constraints.join(' AND ')}
             ORDER BY participant.role,
                      forename,
                      surname,
                      user.id
             LIMIT ?;`;
  parameters.push(filter.limit);
  query = query.replace(/\n/g, ' ').replace(/ +(?= )/g, '');


  pool.query(query, parameters, (err, results, fields) => {
    if (err) {
      console.error(err);
      res.sendStatus(500);
      return next();
    }
    res.json(results);
    next();
  });
});

const commentsFilterValidator = validateRoute({
  query: {
    type: 'object',
    properties: {
      filter: {
        type: 'object',
        properties: {
          parent: { type: 'integer', minimum: 0 },
          limit: { type: 'integer', minimum: 1, maximum: db.QUERY_LIMIT_MAX },
          excluding: { // comment IDs, typically already loaded participants
            type: 'array',
            items: { type: 'integer', minimum: 0 }
          }
        }
      }
    }
  }
});
router.get('/:id/comments', queryStringParser,
  commentsFilterValidator,
  (req, res, next) => {
  /* Fetch a list of comments on a project
   *
   * Response:
   * [
   *   {
   *     id: integer > 0
   *     published_date: ISO datetime (YYYY-MM-DDTHH:MM:SSZ)
   *     parent: integer > 0
   *     answer_count: integer
   *     author_forename: string
   *     picture: url
   *     content: string
   *   }
   * ]
   */

  // Get filter parameter
  let filter = req.query.filter || {};

  // Fill filter defaults
  filter.limit = filter.limit || db.QUERY_LIMIT_MAX;


  // Parse filter
  let constraints = ['comment.project_id = ?'];
  let parameters = [res.locals.id];

  if (typeof filter.parent !== 'undefined') {
    let query = `comment.id IN
                 ( WITH RECURSIVE childrenTable AS
                   ( SELECT *
                       FROM comment
                       WHERE parent_id = ?
                     UNION ALL
                     SELECT comment.*
                       FROM comment
                         INNER JOIN childrenTable
                         ON comment.parent_id = childrenTable.id
                   ) SELECT id
                       FROM childrenTable )`
    constraints.push(query);
    parameters.push(filter.parent);
  }

  if (filter.excluding && filter.excluding.length) {
    constraints.push(`comment.id NOT IN (?)`);
    parameters.push(filter.excluding);
  }

  let query = `SELECT comment.id,
                      comment.posted_at AS published_date,
                      comment.parent_id AS parent,
                      user.firstName AS author_forename,
                      user.photo AS picture,
                      comment.content,
                      COUNT(children.id) AS answer_count
                 FROM comment
                   INNER JOIN user
                   ON comment.user_id = user.id
                   LEFT JOIN comment children
                   ON comment.id = children.parent_id
                 WHERE ${constraints.join(' AND ')}
                 GROUP BY comment.id
                 ORDER BY comment.posted_at DESC
                 LIMIT ?;`;
  parameters.push(filter.limit);
  query = query.replace(/\n/g, ' ').replace(/ +(?= )/g, '');

  pool.query(query, parameters, (err, results, fields) => {
    if (err)
      return next(err);

    res.json(results);
    next();
  });
});

const createProjectValidator = validateRoute({
  body: {
    type: 'object',
    properties: {
      name: { type: 'string', pattern: patterns.words },
      type: { type: 'string', pattern: patterns.projectType },
      sector: { type: 'string', pattern: patterns.sector },
      short_description: { type: 'string' }, // no pattern, beware SQLi & XSS!
      theme: {
        type: 'array',
        minItems: 1,
        uniqueItems: true,
        items: {
          type: 'string',
          pattern: patterns.words
        }
      },
      duration: { type: 'integer', minimum: 0 },
      details: { type: 'string' }, // no pattern, beware SQLi & XSS!
      goal: { type: 'string' }, // no pattern, beware SQLi & XSS!
      technos: {
        type: 'array',
        minItems: 1,
        uniqueItems: true,
        items: {
          type: 'string',
          pattern: patterns.words
        }
      },
      status: { type: 'string', pattern: patterns.status },
      roles: { type: 'string', pattern: patterns.words },
      begin_date: { type: 'string', pattern: patterns.datetime }
    },
    required: [
      'name',
      'type',
      'short_description',
      'theme',
      'status'
    ],
    additionalProperties: false
  }
});
router.post('/new', createProjectValidator, (req, res, next) => {
   /* Create a new project */

  let project = req.body;
  project.publishedAt = new Date();
  project.user_id = req.session.user.id;

  let hasTechnology = (project.technos && project.technos.length);


  const query = util.promisify(pool.query).bind(pool);
  (async () => {
    // Create theme records
    try {
      await query(`INSERT INTO theme (name) VALUES ${project.theme.map(_ => '(?)').join(', ')} ON DUPLICATE KEY UPDATE name = name;`, project.theme);
    } catch (err) {
      return next(err);
    }

    // Create technology records
    if (hasTechnology) {
      try {
        await query(`INSERT INTO technology (name) VALUES ${project.technos.map(_ => '(?)').join(', ')} ON DUPLICATE KEY UPDATE name = name;`, project.technos);
      } catch (err) {
        return next(err);
      }
    }

    // Get IDs
    let themeId = []
    try {
      themeId = (await query('SELECT id FROM theme WHERE name IN (?);', [ project.theme ])).map(row => row.id);
    } catch (err) {
      return next(err);
    }

    let techId = [];
    if (hasTechnology) {
      try {
        techId = (await query('SELECT id FROM technology WHERE name IN (?);', [ project.technos ])).map(row => row.id);
      } catch (err) {
        return next(err);
      }
    }

    delete project.theme;
    delete project.technos;

    // Add project to database
    pool.getConnection((err, connection) => {
      if (err)
        return next(err);

      connection.beginTransaction(err => {
        if (err)
          return next(err);

        // Add project
        (async () => {
          try {
            // Insert project
            project.id = (await query('INSERT INTO project SET ?;', project)).insertId;

            // Insert themes
            await query(`INSERT INTO project_theme (project_id, theme_id) VALUES ${themeId.map(_ => '(?)').join(', ')};`, themeId.map(theme => [ project.id, theme ]));

            // Insert technologies
            if (hasTechnology) {
              await query(`INSERT INTO project_technology (project_id, technology_id) VALUES ${techId.map(_ => '(?)').join(', ')};`, techId.map(tech => [ project.id, tech ]));
            }
          } catch (err) {
            connection.rollback(() => connection.release());
            return next(err);
          }

          connection.commit(err => {
            if (err) {
              connection.rollback(() => connection.release());
              return next(err);
            }
            connection.release();
          });

          res.status(201).json({
            project: project
          });
        })();
      });
    });
  })();
});


router.post('/:id/upvote', (req, res, next) => {
   /* Upvote a project */
  vote(req, res, next, 1);
});

router.post('/:id/downvote', (req, res, next) => {
   /* Upvote a project */
  vote(req, res, next, -1);
});

router.post('/:id/resetvote', (req, res, next) => {
   /* Reset a vote */
  vote(req, res, next, 0);
});

router.post('/:id/subscribe', (req, res, next) => {
  /* Subscribe to a project */
  pool.query('INSERT INTO subscription (user_id, project_id) VALUES (?, ?) ON DUPLICATE KEY UPDATE project_id = project_id, user_id = user_id;', [
    req.session.user.id,
    res.locals.id,
  ], (err, results, fields) => {
    if (err)
      return next(err);
    res.sendStatus(200);
  });
});

router.post('/:id/unsubscribe', (req, res, next) => {
  /* Unsubscribe to a project */
  pool.query('DELETE FROM subscription WHERE user_id = ? AND project_id = ?;',
    [ req.session.user.id, res.locals.id ],
    (err, results, fields) => {
      if (err)
        return next(err);
      res.sendStatus(200);
    });
});

router.post('/:id/report', (req, res, next) => {
  /* Report a project */
  if (!req.session.user) {
    res.sendStatus(401);
    return next();
  }

  pool.query('UPDATE project SET reported = reported + 1 WHERE id = ?;', [
    res.locals.id,
  ], (err, results, fields) => {
    if (err) {
      res.sendStatus(500);
      return next();
    }

    res.sendStatus(201);
    next();
  });
});

router.post('/:id/delete', (req, res, next) => {
  /* Delete a project */

  // Check that author is the connected user
  pool.query('SELECT user_id FROM project WHERE id = ?;', [ res.locals.id ], (err, results, fields) => {
    if (err)
      return next(err);

    if (!results.length)
      return res.sendStatus(404);

    if (req.session.user.id !== results[0].user_id)
      return res.sendStatus(403);

    pool.query('DELETE FROM project WHERE id = ?;', [ res.locals.id ], (err, results, fields) => {
      if (err)
        return next(err);

      res.sendStatus(200);
    });
  });
})


const createCommentValidator = validateRoute({
  body: {
    type: 'object',
    properties: {
      content: { type: 'string' } // no pattern, beware SQLi & XSS!
    },
    required: [ 'content' ],
    additionalProperties: false
  }
});
router.post('/:id/comment/:parent?', createCommentValidator, (req, res, next) => {
   /* Comment a project */

  let comment = {
    content: req.body.content,
    project_id: res.locals.id,
    user_id: req.session.user.id,
    posted_at: new Date(),
    parent_id: undefined
  };
  comment.parent_id = res.locals.parent || null;

  pool.query('INSERT INTO comment SET ?;', comment, (err, results, fields) => {
    if (err)
      return next(err);
    res.sendStatus(201);
  });
});

router.post('/:id/add-participant/:email', (req, res, next) => {
  let participant = {
    project_id: res.locals.id,
    role: "participant"
  };

  pool.query('SELECT user_id FROM project WHERE id = ?;', [ res.locals.id ], (err, results, fields) => {
    if (err)
      return next(err);

    if (!results.length)
      return res.sendStatus(404);

    if (req.session.user.id !== results[0].user_id)
      return res.sendStatus(403);


    pool.query('SELECT id FROM user WHERE email = ?', [res.locals.email], (err, results, fields) => {
      if (err)
        return next(err);

      if (results.length === 0) {
        res.sendStatus(400);
        return next();
      }
      participant.user_id = results[0].id;

      pool.query('INSERT INTO participant SET ?', participant, (err, resuls, fields) => {
        if (err)
          return next(err);

        res.sendStatus(201);

      });
    });
  });

});

function vote(req, res, next, value) {
   pool.query('INSERT INTO vote (user_id, project_id, value, date) VALUES (?, ?, ?, NOW()) ON DUPLICATE KEY UPDATE value = ?, date = NOW();', [
       req.session.user.id,
       res.locals.id,
       value,
       value
   ], (err, results, fields) => {
      if (err)
         return next(err);

      pool.query('SELECT SUM(value) as total FROM vote WHERE project_id = ?', [res.locals.id], (err, results, fields) => {
        if (err)
          return next(err);

        res.status(201).json({
          vote: results[0].total
        });
        next();

      });
   });
}

module.exports = router;
