const unicodeLetter = require('./unicode-General_Category=Letter-codepoints');

module.exports = {
  datetime:    '^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])$',
  projectType: '^(PA|PP|UV|TX|PR)$',
  role:        '^(author|leader|participant)$',
  sector:      '^(HuTech|TC|GI|IM|GU|GB|GP)$',
  sort_key:    '^(vote|publication)$',
  sort_order:  '^(ascending|descending)$',
  status:      '^(idea|definition|specification|conception|development|test|production)$',
  words:       `^([!-~]|${unicodeLetter})(([ -~]|${unicodeLetter})*([!-~]|${unicodeLetter}))?\$`, // loose pattern, beware SQLi & XSS!
};
