const db = {
  QUERY_LIMIT_MAX: 20,
  QUERY_LIMIT_DEFAULT: 4
};

const cas = {
  BASE_URL: 'https://cas.utc.fr/cas',
  LOGIN_ROUTE: '/login?service=',
  VALIDATE_ROUTE: '/serviceValidate?service='
};

module.exports = {
  db: db,
  cas: cas
};
