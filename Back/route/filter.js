const defaults = {
  match: 'user_id',
  secondaryMatch: 'project_id',
  table: 'project',
  secondaryTable: undefined,
  role: undefined
};

const updateConstraints = (constraints, query, table) => {
  constraints.length = 0;
  constraints.push(`${table}.id IN ${query}`);
};

const countMinMax = (constraints, parameters, filter, options) => {
  options = Object.assign({}, defaults, options);

  if (typeof options.role !== 'undefined') {
    constraints.push(`${options.secondaryTable}.role = ?`);
    parameters.push(options.role);
  }

  let havings = [];
  if (typeof filter.min !== 'undefined'){
    havings.push(`COUNT(${options.secondaryTable}.${options.match}) >= ?`);
    parameters.push(filter.min);
  }
  if (typeof filter.max !== 'undefined'){
    havings.push(`COUNT(${options.secondaryTable}.${options.match}) <= ?`);
    parameters.push(filter.max);
  }

  let query = `(SELECT ${options.table}.id
                  FROM ${options.table}
                    LEFT JOIN ${options.secondaryTable}
                    ON ${options.table}.id = ${options.secondaryTable}.${options.secondaryMatch}`;
  if (constraints.length)
    query +=    ` WHERE ${constraints.join(' AND ')}`;
  query +=      ` GROUP BY ${options.table}.id
                    HAVING ${havings.join(' AND ')})`;

  updateConstraints(constraints, query, options.table);
};

const sumMinMax = (constraints, parameters, filter, options) => {
  options = Object.assign({}, defaults, options);

  let havings = [];
  if (typeof filter.min !== 'undefined'){
    havings.push(`COALESCE(SUM(${options.secondaryTable}.${options.match}), 0) >= ?`);
    parameters.push(filter.min);
  }
  if (typeof filter.max !== 'undefined') {
    havings.push(`COALESCE(SUM(${options.secondaryTable}.${options.match}), 0) <= ?`);
    parameters.push(filter.max);
  }

  let query = `(SELECT ${options.table}.id
                  FROM ${options.table}
                    LEFT JOIN ${options.secondaryTable}
                    ON ${options.table}.id = ${options.secondaryTable}.${options.secondaryMatch}`;
  if (constraints.length)
    query +=    ` WHERE ${constraints.join(' AND ')}`;
  query +=      ` GROUP BY ${options.table}.id
                    HAVING ${havings.join(' AND ')})`;

  updateConstraints(constraints, query, options.table);
};

const matchAny = (constraints, parameters, filter, options) => {
  options = Object.assign({}, defaults, options);

  if (typeof options.role !== 'undefined') {
    constraints.push(`${options.secondaryTable}.role = ?`);
    parameters.push(options.role);
  }

  if (typeof options.secondaryTable !== 'undefined') {
    if (filter.including_any && filter.including_any.length) {
      constraints.push(`${options.secondaryTable}.${options.match} IN (?)`);
      parameters.push(filter.including_any);
    }
    if (filter.excluding_any && filter.excluding_any.length) {
      constraints.push(`${options.secondaryTable}.${options.match} NOT IN (?)`);
      parameters.push(filter.excluding_any);
    }
  } else {
    if (filter.including_any && filter.including_any.length) {
      constraints.push(`${options.table}.${options.match} IN (?)`);
      parameters.push(filter.including_any);
    }
    if (filter.excluding_any && filter.excluding_any.length) {
      constraints.push(`${options.table}.${options.match} NOT IN (?)`);
      parameters.push(filer.excluding_any);
    }
  }

  let query;
  if (typeof options.secondaryTable !== 'undefined') {
    query = `(SELECT ${options.table}.id
                FROM ${options.table}
                  LEFT JOIN ${options.secondaryTable}
                  ON ${options.table}.id = ${options.secondaryTable}.${options.secondaryMatch}
                WHERE ${constraints.join(' AND ')})`;
  } else {
    query = `(SELECT ${options.table}.id
                FROM ${options.table}
                WHERE ${constraints.join(' AND ')})`;
  }

  updateConstraints(constraints, query, options.table);
};

const matchAll = (constraints, parameters, filter, options) => {
  options = Object.assign({}, defaults, options);

  let query;

  /* Including */
  if (typeof options.role !== 'undefined') {
    constraints.push(`${options.secondaryTable}.role = ?`);
    parameters.push(options.role);
  }

  if (filter.including_all && filter.including_all.length) {
    constraints.push(`${options.secondaryTable}.${options.match} IN (?)`);
    parameters.push(filter.including_all);
    query = `(SELECT ${options.table}.id
                    FROM ${options.table}
                      LEFT JOIN ${options.secondaryTable}
                      ON ${options.table}.id = ${options.secondaryTable}.${options.secondaryMatch}
                    WHERE ${constraints.join(' AND ')}
                    GROUP BY ${options.table}.id
                      HAVING COUNT(*) = ?)`;
    parameters.push(filter.including_all.length);

    updateConstraints(constraints, query, options.table);
  }

  /* Excluding */
  if (typeof options.role !== 'undefined') {
    constraints.push(`${options.secondaryTable}.role = ?`);
    parameters.push(options.role);
  }

  if (filter.excluding_all) {
    constraints.push(`${options.secondaryTable}.${options.match} IN (?)`);
    parameters.push(filter.excluding_all);
    query = `(SELECT ${options.table}.id
                    FROM ${options.table}
                      LEFT JOIN ${options.secondaryTable}
                      ON ${options.table}.id = ${options.secondaryTable}.${options.secondaryMatch}
                    WHERE ${constraints.join(' AND ')}
                    GROUP BY ${options.table}.id
                      HAVING COUNT(*) <> ?)`;
    parameters.push(filter.excluding_all.length);

    updateConstraints(constraints, query, options.table);
  }
};

module.exports = {
  countMinMax,
  sumMinMax,
  matchAny,
  matchAll
}
