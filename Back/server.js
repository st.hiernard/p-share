const pool = require('./db');
const { cookie } = require('./config');

const express = require('express');
const session = require('express-session');
const path = require('path');
const redis = require('redis');
const redisClient = redis.createClient(cookie.store.port, cookie.store.host);
redisClient.auth(cookie.store.auth_pass);
const redisStore = require('connect-redis')(session);
const bodyParser = require('body-parser');
cookie.store.client = redisClient;

const auth = require('./middleware/auth');

const routerProject = require('./route/project');
const routerUser = require('./route/user');
const routerAuth = require('./route/auth');

let app = express();

/* Global configuratioin */
app.set('x-powered-by', false);

/* Session */
redisClient.on('connect', function() {
    console.log('Redis client connected');
});
redisClient.on('error', (err) => {
  console.log('Redis error: ', err);
});

app.use(session({
  cookie: {
    maxAge: 60 * 60 * 1000,
    path: '/',
    sameSite: true,
    secure: cookie.secure
  },
  name: 'p-share_session',
  saveUninitialized: false,
  secret: cookie.secret,
  store: new redisStore({client: redisClient}),
  resave: false
}));

/* Parameters parsing */
// For POST only, as xhr does not support bodies in GET requests
//   see https://xhr.spec.whatwg.org/#the-send()-method :
//   > If the request method is GET or HEAD, set body to null.
app.use(express.json());

/* Routing */
app.use('/api/project', auth, routerProject);
app.use('/api/user', auth, routerUser);
app.use('/api/auth', routerAuth);

const PORT = process.env.backend_port || 5000;

app.listen(PORT, () => console.log(`\x1b[34mExpress server started on port ${PORT}\x1b[0m`));
