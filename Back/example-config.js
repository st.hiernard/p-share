const db_credentials = {
  host:     '< server hostname >',
  database: '< database name >',
  user:     '< MySQL user >',
  password: '< MySQL password >'
};

const cookie = {
  secret: '< session cookie secret >',
  secure: false,
  store: {
    host:      '< redis hostname >',
    port:      6379,
    ttl:       24 * 60 * 60,
    auth_pass: '< session store password >'
  }
}

module.exports = {
  db: db_credentials,
  cookie: cookie
};
